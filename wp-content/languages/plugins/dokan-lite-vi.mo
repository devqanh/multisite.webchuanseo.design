��    &      L      |      |     }  1   �     �     �     �              
   (  
   3     >     N     \  	   j  	   t  -   ~     �     �  #   �     �          (  %   H     n     ~     �     �  5   �  	   �     �     �     �  )        1     @     P  A   \  ,   �  �  �     �  D   �      	     4	     H	     a	      h	     �	     �	     �	  #   �	     �	     �	     
  >   
     \
     q
  (   �
     �
     �
     �
  (        9     G     M     ^  7   j     �     �  0   �       #        6     P     a  N   |  7   �   Confirm New Password Current Password (leave blank to leave unchanged) Edit Account Details Email address Email sent successfully! Error! Error: Nonce verification failed First Name First name I am a customer I am a vendor Invalid email Last Name Last name New Password (leave blank to leave unchanged) Password Change Phone Number Please enter a valid email address. Please enter your first name. Please enter your last name. Please enter your phone number. Please provide a valid email address. Profile Picture Save Save changes Settings Settings <i class="fa fa-angle-right pull-right"></i> Shop Name Shop URL Show email address in store Success! This email address is already registered. Unknown error! Update Settings Visit Store Your account is not enabled for selling, please contact the admin Your information has been saved successfully Project-Id-Version: Dokan 2.7.2
Report-Msgid-Bugs-To: https://wedevs.com/contact/
POT-Creation-Date: 2017-12-18 08:15:46+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2018-02-01 14:26+0000
Last-Translator: admin <admin@multisite.webchuanseo.design>
Language-Team: Vietnamese
Language: vi
Plural-Forms: nplurals=1; plural=0
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco - https://localise.biz/ Xác nhận mật khẩu Mật khẩu hiện tại ( để trống nếu không thay đổi ) Sửa tài khoản  Địa chỉ email  Gửi email thành công Lỗi! Error: Nonce verification failed Tên của bạn Tên của bạn Tôi là khách hàng Tôi là đại lý thương hiệu Email không đúng Họ của bạn  Họ của bạn  Mật khẩu mới ( để trống nếu không thay đổi  ) Đổi mật khẩu  Nhập số điện thoại Xin nhập địa chỉ mail chính xác Xin nhập tên của bạn  Xin nhập họ của bạn  Xin nhập số điện thoại Xin nhập địa chỉ mail chính xác Ảnh profile Lưu  Lưu thay đổi Cài đặt Cài đặt<i class="fa fa-angle-right pull-right"></i> Tên cửa hàng  Địa chỉ shop ( Shop url ) Hiển thị địa chỉ mail trên cửa hàng Thành công! Email này đã bị đăng ký :(  Lỗi không xác định Lưu thay đổi Thương hiệu của tôi Tài khoản chưa được kích hoạt , liên hệ với quản trị viên Thông tin của bạn đã được lưu thành công! 