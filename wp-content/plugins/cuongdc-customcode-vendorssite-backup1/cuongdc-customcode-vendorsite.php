<?php
/* Plugin Name: CuongDC Custom code cho vendor site
 * Description: Thêm các chức năng cho site này, A project of CuongDCDev@gmail.com
 * Author: CuongDcDev@Gmail.com
 * Text Domain: cuongdc_vendor
 */

/**
 * Plugin này bao gồm CFS và Option Screen
 */
define('CUONGDC_VENDOR_DOMAIN' , 'cuongdc_vendor' );
define('CUONGDC_VENDOR_DIR' , dirname( __FILE__ ) );
//custom field suite
require_once( CUONGDC_VENDOR_DIR . '/lib/custom-field-suite/cfs.php' );
//option screens for CFS
require_once ( CUONGDC_VENDOR_DIR . '/lib/cfs-option-screens/cfs-options-screens.php' );
//advanced custom field for CFS
require_once ( CUONGDC_VENDOR_DIR . '/lib/cfs-advanced-text-field/cfs-advanced-text-field.php' );
//load option page
require_once( CUONGDC_VENDOR_DIR . '/option/option-page.php' );
//adding custom column to user table admin
require_once( CUONGDC_VENDOR_DIR  . '/option/user-table-custom.php' );

//shortcodes
require_once( CUONGDC_VENDOR_DIR  .'/option/shortcode.php' );
//cac function linh tinh 
require_once( CUONGDC_VENDOR_DIR  . '/option/mix.php' );

//register assets

add_action('wp_enqueue_scripts' , function(){
   wp_enqueue_script("cuongdcpluginjs" ,  plugin_dir_url(__FILE__) . "asset/main.js" , 'jquery' , '1.0' , true  );
});
