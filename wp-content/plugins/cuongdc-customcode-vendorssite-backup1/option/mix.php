<?php
/*
 * A project of CuongDCDev@gmail.com
 * Mấy customize nhỏ nhỏ 
 */

add_filter('cfs_user_display', function($userLoginName, $userId, $fields) {
    $shop_name = get_user_meta($userId, 'dokan_store_name', true);
    return !empty($shop_name) ? $userLoginName . " - <b style='color:red'>" . $shop_name . "</b>" : $userLoginName;
}, 90, 3);

//filter hien thi ten shop khi o page user 
add_filter( 'wp_title' , function($title, $sep){
        if ( dokan_is_store_page() ) {

          //  return "ahihi";
            $site_title = get_bloginfo( 'name' );
            $store_user = get_userdata( get_query_var( 'author' ) );
            $store_info = dokan_get_store_info( $store_user->ID );
            $store_name = esc_html( $store_info['store_name'] );
            $title      = "$store_name $sep $site_title";

            // // Add a page number if necessary.
            // if ( $paged >= 2 || $page >= 2 ) {
            //     $title = "$title $sep " . sprintf( __( 'Page %s', 'dokan-lite' ), max( $paged, $page ) );
            // }

            return $title;
        }
    return $title;

}, 99 , 2  );
// Add Facebook url
add_action('wp_footer', function() {
    ?>
    <div id="fb-root"></div>
    <script>(function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = 'https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.11&appId=930641410378507';
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
    <?php
});

/**
 * 
 * @return Trả về mảng chưa list thương hiệu
 * [ thuong_hieu ] = [
 *           anh_loai_thuong_hieu  ,
 *            ten_loai_thuong_hieu ,
 *             gioi_thieu_loai_thuong_hieu ,
 *               hinh_nen ,
 *               vi_tri_xuat_hien ,
 *               thuong_hieu_noi_bat ,
 *                thuong_hieu_trong_danh_sach 
 * ]
 */
function cuongdc_get_list_thuonghieu() {
    return cfs_get_option('shop_brand_option', 'thuong_hieu');
}

function cuongdc_get_qc_thuonghieu(){
     return cfs_get_option('shop_brand_option', 'quang_cao_trang_don');
}
/**
 * @return Trả về mảng chứa list user dc phép đặt quảng cáo
 */
function cuongdc_get_list_user_ads() {
    return cfs_get_option('shop_brand_option', 'thuong_hieu_duoc_phep_dat_quang_cao');
}

/**
 * @return Trả về mảng chứa list user trong 1 thương hiệu theo tên thương hiệu, false neu ko tim thay 
 */
function cuongdc_get_thuonghieu_list_by_name($tenThuongHieu) {
    $listThuongHieu = cuongdc_get_list_thuonghieu();
    $tenThuongHieu = cuongdc_make_slug( $tenThuongHieu );
    foreach ($listThuongHieu as $thuongHieu ) {
        if ( strcmp( $tenThuongHieu, cuongdc_make_slug($thuongHieu['ten_loai_thuong_hieu']) ) == 0) {
            return $thuongHieu;
        }
    }
    return false;
}

//tra ve ten dep thong qua slug 
function cuongdc_get_ten_thuonghieu_list_by_slug($slug){
      $listThuongHieu = cuongdc_get_list_thuonghieu();
    foreach ($listThuongHieu as $thuongHieu ) {
        if ( strcmp( $slug, cuongdc_make_slug($thuongHieu['ten_loai_thuong_hieu']) ) == 0) {
            return $thuongHieu['ten_loai_thuong_hieu'];
        }
    }
    return 'Không tìm thấy :( ';
}
/**
 * Trả về thông tin của hàng đó theo id
 * @return array [
            'store_name' => 'ten cua hang ',
            'store_settings' => array(  )
    ]
 */

function cuongdc_get_user_thuonghieu_by_id(  $uid ){
    $user = get_user_by( 'ID' , $uid );
    return array(
        'store_slug' => trim($user->user_login) ,
        'store_settings' => get_user_meta( $uid , 'dokan_profile_settings' , true )
    );
}


/**
 * @return trả về nội dung Shop email lấy từ config 
 */
function cuongdc_get_shop_profile_update_email_template() {
    return array(
        'title' => cfs_get_option('shop_brand_option', 'email_shop_profile_updated_template_title'),
        'content' => cfs_get_option('shop_brand_option', 'email_shop_profile_updated_template_content')
    );
}

/**
 * @return Trả về nội dung cập nhật email user lấy từ config 
 */
function cuongdc_get_user_profile_update_email_template(){
    return array(
        'title' => cfs_get_option('shop_brand_option', 'email_user_profile_updated_template_title'),
        'content' => cfs_get_option('shop_brand_option', 'email_user_profile_updated_template_content')
    );
}



// Sau khi brand profile  dc update, gui email cho nguoi dung day
add_action('dokan_store_profile_saved', function( $user_id, $settings ) {
    $user = get_userdata($user_id);
    if (!empty($user->user_email)) {
        $email_template = cuongdc_get_shop_profile_update_email_template();
        wp_mail($user->user_email, $email_template['title'], $email_template['content']);
    }
}, 90, 2);

//Sau khi user profile da được update, gui email lai cho user day 

add_action('woocommerce_save_account_details', function($user_id) {
    $user = get_userdata($user_id);
    // var_dump( $user->user_email ); die;
    if (!empty($user->user_email)) {
        $email_template = cuongdc_get_user_profile_update_email_template();
        wp_mail($user->user_email, $email_template['title'], $email_template['content']);
    }
}, 90, 1);


//create slug from string 

function cuongdc_make_slug($string, $replace = array(), $delimiter = '-') {
  // https://github.com/phalcon/incubator/blob/master/Library/Phalcon/Utils/Slug.php
  if (!extension_loaded('iconv')) {
    throw new Exception('iconv module not loaded');
  }
  // Save the old locale and set the new locale to UTF-8
  $oldLocale = setlocale(LC_ALL, '0');
  setlocale(LC_ALL, 'en_US.UTF-8');
  $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
  if (!empty($replace)) {
    $clean = str_replace((array) $replace, ' ', $clean);
  }
  $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
  $clean = strtolower($clean);
  $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
  $clean = trim($clean, $delimiter);
  // Revert back to the old locale
  setlocale(LC_ALL, $oldLocale);
  return $clean;
}

function cuongdc_trim_strip( $string ){
    return trim( strip_tags($string) );
}


//add custom tag cho contact form 7 
add_action( 'wpcf7_init', function(){
    wpcf7_add_form_tag("uid" , 'cuongdc_set_uid', array('name-attr' => true ));
} );

function cuongdc_set_uid($tag){
    $user_target_id = '';
    if(dokan_is_store_page() ){
         $store_user =  get_userdata(get_query_var('author') );
           $user_target_id = $store_user->ID;
           
    }
    $atts = array(
        'type' => 'hidden' , 
        'name' => 'uid',
        'value' => $user_target_id
    );
    $xinput = sprintf( '<input %s/>' , wpcf7_format_atts( $atts ) );
    return $xinput;
}


add_action( 'template_redirect' , function($template){
   $site_url = get_site_url() ;
   $url =   (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
   if( $url == $site_url . "/dashboard/"  ){
       wp_redirect(  get_site_url() . "/dashboard/settings/store/");
   }
//   die( "url : " . $url  . " ---- " .  get_site_url() . "/dashboard/"  );
   return $template;
} );


//allow iframe
function cuongdc_send_frame_options_header() {
	@header( 'X-Frame-Options: SAMEORIGIN' );
}
add_action( 'login_init', 'cuongdc_send_frame_options_header', 10, 0 );
add_action( 'admin_init', 'cuongdc_send_frame_options_header', 10, 0 );


//remove menu if seller role
add_action('admin_head', 'wpse_52099_script_enqueuer');
function wpse_52099_script_enqueuer(){
    if( current_user_can('seller') ) {
        echo <<<HTML
        <style type="text/css">
        #wpcontent, #footer { margin-left: 0px; }
        #adminmenumain, #mw_adminimize_admin_bar, .wp-heading-inline{
            display:none !important;
        }
        
        #wpcontent{
            margin-left:0px !important;
        }
        </style>
        <script type="text/javascript">
        jQuery(document).ready( function($) {
            $('#adminmenumain, #adminmenuwrap').remove();
            $('#wpcontent').css('width:100%');
        });     
        </script>
HTML;
    }
}

