<?php
/*
 * A project of CuongDCDev@gmail.com
 * Hien thi shortcode list o trang chu
 */

add_shortcode('trang-chu-list-thuong-hieu', 'cuongdc_register_shortcode_thuonghieu_home');

function cuongdc_register_shortcode_thuonghieu_home($args) {
    $thuongHieuList = cuongdc_get_list_thuonghieu();

    usort($thuongHieuList, function($a, $b) {
        return $a['vi_tri_xuat_hien'] < $b['vi_tri_xuat_hien'];
    });

    ob_start();
    //var_dump($thuongHieuList);
    for ($i = 0; $i < sizeof($thuongHieuList); $i++):
        $thuongHieu = $thuongHieuList[$i];

        ?>

        <section class="section block3 home-list-thuong-hieu" id="thuong-hieu-<?php echo $i ?>" style="padding-top: 30px; padding-bottom: 30px;">

            <div class="bg section-bg fill bg-fill   bg-loaded" style="background-image: url(<?php echo $thuongHieu['hinh_nen']; ?>);"> </div>
            <!-- .section-bg -->

            <div class="section-content relative">

                <div class="row" id="row-1865682851">
                    <div class="col medium-3 small-12 large-3">
                        <div class="col-inner">
                            <a href="#<?php ?>" title="<?php echo $thuongHieu['ten_loai_thuong_hieu'] ?>">
                                <div class="img has-hover x md-x lg-x y md-y lg-y" id="">
                                    <div class="img-inner dark">
                                       <a href="/brands?t=<?php echo cuongdc_make_slug( $thuongHieu['ten_loai_thuong_hieu'] ) ?>">
                                        <img src="<?php echo $thuongHieu['anh_loai_thuong_hieu']; ?>" 
                                             class="attachment-large size-large"
                                             width="226" height="612">
                                        </a>
                                    </div>
                                </div>
                            </a>

                        </div>
                    </div><!-- Thuong hieu avatar -->


                    <div class="col medium-9 small-12 large-9">
                        <div class="col-inner">
                            <div class="row row-custom list-item has-block tooltipstered">

                                <!--START LIST THUONG HIEU-->
                                <?php
                                //for( $i = 0 ; $i < 8 ; $i++ )
                                
                                   $id = 0 ; 
                                    foreach ($thuongHieu['thuong_hieu_noi_bat'] as $userId):
                                    $id++;
                                    $userShopProfile = get_user_meta($userId, 'dokan_profile_settings', true);
                                    $userShopName = get_user_meta($userId, 'dokan_store_name', true);
                                    $user = get_user_by('id' , $userId );
                                    if( empty( $userShopProfile ) || empty( $userShopName ) ) continue;
                                    
                                    //demo mode, duplicate x16
                                    for( $d = 0 ; $d < 12 ; $d++ ):
                                    ?>
                                    <div class="col medium-3  small-6 large-3" id="thuonghieu-<?php echo $id?>" >
                                        <a href="/b/<?php echo trim( $user->user_login );  ?>" title="<?php echo $userShopName ?>">
                                          <div class="content-item">
                                               <?php echo wp_get_attachment_image($userShopProfile['gravatar'], 'normal') ?>
                                              <div class="text-content-item">
                                                  <h5><?php echo $userShopName; ?></h5>
                                                  
                                                  <p>Chi phí tham gia: <?php echo  !empty( trim($userShopProfile['shop_money_start']) ) ? wp_trim_words(trim($userShopProfile['shop_money_start']), '12' , '...' ) : ' Liên hệ ' ?></p>
                                              </div>
                                          </div>
                                        </a>
                                    </div>
                                   <?php 
                                   endfor;//demo mode
                                   endforeach; ?>
                                <!-- END LIST THUONG HIEU--> 
                            </div><!-- END LIST THUONG HIEU--> 
                            <?php 
                                //var_dump( $thuongHieu['thuong_hieu_noi_bat']  );
                            ?>
                            <?php if( !empty( $thuongHieu['thuong_hieu_noi_bat'] ) && intval(  $thuongHieu['thuong_hieu_noi_bat'][0] ) > 0 ):?>
                                 <div class="all-more">
                                <h1><a href="/brands?t=<?php echo cuongdc_make_slug( $thuongHieu['ten_loai_thuong_hieu'] )  ?>">[XEM TẤT CẢ]</a></h1>
                            </div>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .section-content -->
        </section>

        <?php
    endfor;
    ?>

    <?php
    return ob_get_clean();
}//short code [trang-chu-list-thuong-hieu]
