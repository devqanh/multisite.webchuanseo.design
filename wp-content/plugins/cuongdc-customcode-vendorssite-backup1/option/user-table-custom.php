<?php

/*
 * A project of CuongDCDev@gmail.com
 * adding a custom to user table
 */

add_action('manage_users_columns', function($column_headers) {
    var_dump($column_headers);
    unset($column_headers['posts']);
    unset($column_headers['name']);

    //add ten gian hang user nay so huu
    $column_headers['shop_name'] = 'Tên thương hiệu';
    return $column_headers;
});

add_action('manage_users_custom_column', function( $value, $columnName, $userId ) {
    if ($columnName == 'shop_name') {
        $shop_name = get_user_meta($userId , 'dokan_store_name' ,true );
        return !empty($shop_name) ? $shop_name : '🎅🎅🎅🎅🎅🎅🎅';
    }
    return $value;
} , 10 , 3);
