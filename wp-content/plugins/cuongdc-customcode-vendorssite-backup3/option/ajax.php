<?php

//ADD TO BOOKMARD AJAX FRONTEND
add_action('wp_ajax_nopriv_cuongdc_add_bookmark' , 'cuongdc_add_bookmark');
add_action('wp_ajax_cuongdc_add_bookmark' , 'cuongdc_add_bookmark');

function cuongdc_add_bookmark(){
    $uid = get_current_user_id();
    $uid_bookmark = $_POST['uid'];
    $bookmarks = get_user_meta( $uid , 'cuongdc_user_bookmarks', true );
    $message = 'login';
    
    if(!is_array($bookmarks) ){
        $bookmarks = array( $bookmarks );
    }
    if( !wp_verify_nonce($_POST['nonce']) ){
        echo 'dmm';
        die;
    }
    if( $uid <= 0  ){
        echo $message;
        die;
    }
    
    if( in_array( $uid_bookmark , $bookmarks ) ){
        unset( $bookmarks[ array_search( $uid_bookmark , $bookmarks ) ] );
        $message = 'removed';
    }else{
        array_push($bookmarks,$uid_bookmark );
        $message = 'ok';
    }
    if(update_user_meta( $uid , 'cuongdc_user_bookmarks', $bookmarks )){
        echo $message;
    }else{
        echo 'Có lỗi xảy ra, xin thử lại sau ';
        var_dump(  $bookmarks );
    }
    die;
}//cuongdc_Add_to_bookmark()

//get bookmark yeu thich
add_action('wp_ajax_nopriv_cuongdc_get_vendor_bookmark_html' , 'cuongdc_get_vendor_bookmark_html');
add_action('wp_ajax_cuongdc_get_vendor_bookmark_html' , 'cuongdc_get_vendor_bookmark_html');

function cuongdc_get_vendor_bookmark_html(){
    if( !wp_verify_nonce($_POST['nonce']) ){
        echo 'dmm';
        die;
    }
    
    $liked_vendors = cuongdc_get_vendor_bookmark();
    if( !empty( $liked_vendors ) ){
        $html = "";
        foreach( $liked_vendors as $vendor_id ){
            $vendor = get_user_by("id", $vendor_id);
            if( !empty( $vendor ) ){
                $vendor_avatar = get_avatar_url( $vendor_id );
                $vendor_name   = get_user_meta( $vendor_id , "dokan_store_name" , true );
                $vendor_url = get_site_url() . "/b/" . $vendor->user_login;
                $html.=sprintf(
                    '<tr>'
                     . '<td>'
                        . '<a class="user-avatar" href="%s">'
                        . '<img src="%s" title="%s"/><center>%s</center>'
                        . '</a>'
                    . '</td>'
                        . '<td>'
                            . '<a title="bỏ thích thương hiệu này" class="unlike" data-uid="%s"><i class="fa fa-star"></i></a></td>'
                        . '</td>'
                . '</tr>' ,$vendor_url , $vendor_avatar , $vendor_name,$vendor_name , $vendor_id );
            }
        }
        echo $html;
    }else{
        echo "none";
    }
    die;
    
}//vendor bookmark html 


add_action('wp_ajax_nopriv_cuongdc_inbox_send' , 'cuongdc_inbox_send');
add_action('wp_ajax_cuongdc_inbox_send' , 'cuongdc_inbox_send');


function cuongdc_inbox_send(){
    
    if( $_POST['type'] == 'markAsRead' ){
        if(wp_verify_nonce($_POST['nonce']) ){
            $postId = absint($_POST['postId']);
            update_post_meta($postId, 'cuongdc_reply_inbox_read', 'read' ); 
            echo 'ok';
        }else{
            echo 'dmm';
        }

    }//mark as read 
    
    if( $_POST['type'] == 'sendMail' ){
            
        $postId = absint($_POST['postId']);
        $title  = cuongdc_trim_strip( $_POST['title'] );
        $content  = cuongdc_trim_strip( $_POST['content'] );
        $receiver  = cuongdc_trim_strip(get_post_meta($postId, 'wpcf7s_posted-email', true));

        $sent = wp_mail($receiver, $title, $content);
        

        if( $sent ){
            echo 'ok';
            update_post_meta($postId, 'cuongdc_reply_inbox_title', $title);
            update_post_meta($postId, 'cuongdc_reply_inbox_content', $title);
        }else{
            echo 'Không thể gửi email ' ;
        }

        
    }//send mail 
    
    die;
    
    
}