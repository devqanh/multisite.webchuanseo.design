<?php


/**
 * Description of breadcrumbs
 *
 * @author Cường <duongcuong96 at gmail dot com>
 **/


//CHANGE SITE BREAD CRUMBS 


add_filter( 'woocommerce_get_breadcrumb', 'change_breadcrumb', 99 , 1 ); 
function change_breadcrumb( $crumbs ) {
    //Thay đổi trên brand page
    if( dokan_is_store_page() ){
        $uid = get_query_var('author' , false);
        $thuongHieu = cuongdc_get_thuonghieu_by_userid( $uid );
        if( !empty( $thuongHieu ) ){
            $crumbs[1][0] = $thuongHieu['ten_loai_thuong_hieu'];
            $crumbs[1][1] = get_site_url() . '/brands?t=' . cuongdc_make_slug( $thuongHieu['ten_loai_thuong_hieu'] );
        }else{
            $crumbs[1][0] ="#";
            $crumbs[1][1] ="#";
        }

    }
    return $crumbs;
}