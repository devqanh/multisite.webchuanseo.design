<?php 
    add_action('add_meta_boxes' ,'cuongdc_inbox_metabox');
    
    function cuongdc_inbox_metabox_output($post){
        $tieude = get_post_meta($post->ID , 'cuongdc_reply_inbox_title' , true );
        $noidung = get_post_meta($post->ID , 'cuongdc_reply_inbox_content' , true );
        $markAsRead = empty(get_post_meta($post->ID, 'cuongdc_reply_inbox_read' , true )) ? '' : 'checked';
        $receiver = get_post_meta($post->ID , 'wpcf7s_posted-email' , true );
        ?>
<div class="wrap" id="cuongdc-wrap">
<div class="box-markasread">
  
    <div class="field">
          <label>Đánh dấu là đã đọc</label>
        <input type="checkbox"  <?php echo empty( $markAsRead ) ? 'id="mark-as-read" ' : 'checked="checked" onclick="return false"'?> />
    </div>
</div>
    <div class="box-reply">
           
            <div class="field field-ten_loai_thuong_hieu cfs_text">
                 <label>Nhập tiêu đề </label>
                <input id="tieu-de" class="text"  type="text" minlength="15" value="<?php echo !empty($tieude) ? $tieude : '' ?>" >
            </div>
            <label>Nhập nội dung trả lời</label>
            <div class="field  cfs_textarea">
                <textarea id="noi-dung" rows="12" class="textarea-wrap"><?php echo !empty( $noidung ) ? $noidung : '' ?></textarea>
            </div>
            <div class="field">
                <button id="reply" type="button" class="submit-btn button-primary" onclick="return confirm('Xác nhận gửi ? ')">Gửi thư</button>
            </div>
            
             <div class="field">
                 <p><b>Lưu ý:</b> Email này sẽ được gửi thằng vào hộp thư đến của người nhận, trong một số trường hợp, thư này có thể bị gắn là thư rác, 
                    <br/> chúng tôi khuyến khích bạn email trưc tiếp bằng địa chỉ email thương hiệu của bạn với người gửi để tránh thư vào spam </p>
             </div>
    </div>
</div>

<style>
    #cuongdc-wrap input[type=text] , #cuongdc-wrap textarea{
        width: 100%;
        margin:10px 5px;
    }
    
    #mark-as-read{
        transform:scale(1.4);
        
    }
    .box-markasread {
        margin-bottom: 20px;
        font-weight: bold;
        margin-top: 20px;
    }
</style>

<script>
    (function($){
        $(document).ready(function(){
            var noti = new Notyf();
            
            $("#mark-as-read").on("click" , function(){
                $.ajax({
                   url: "<?php echo admin_url('admin-ajax.php');?>",
                   type: "POST",
                   data: {
                    action: 'cuongdc_inbox_send' ,
                    type: "markAsRead",
                    nonce: "<?php echo wp_create_nonce()?>",
                    postId: "<?php echo $post->ID?>",
                   },
                   success: function(data){
                        if( data == "ok" ){
                            noti.confirm("Đã đánh dấu là đã đọc!  ");
                        }else{
                            noti.alert("Có lỗi xảy ra:" + data);
                        }
                   }
                });
            });
            
            $("#reply").on("click" , function(){
                $.ajax({
                    url: "<?php echo admin_url('admin-ajax.php');?>",
                    type: 'POST',
                    data:{
                        action: 'cuongdc_inbox_send' ,
                        type: "sendMail",
                        nonce: "<?php echo wp_create_nonce()?>",
                        title: $("#tieu-de").val(),
                        content: $("#noi-dung").val(),
                        postId: "<?php echo $post->ID?>",
                        
                    },
                    success: function(data){
                        if( data == "ok" ){
                            noti.confirm("Gửi thành công! ");
                        }else{
                            noti.alert("Có lỗi xảy ra:" + data);
                        }
                    }
                    
                });
            });
        });
    })(jQuery);
</script>
<?php 
    }
    
    function cuongdc_inbox_metabox(){
            add_meta_box('cuongdc-inbox-meta', 'Trả lời tin nhắn', 'cuongdc_inbox_metabox_output', 'wpcf7s' , 'normal' ,'default' ); //hien thi meta box cho contact form post type 
    }