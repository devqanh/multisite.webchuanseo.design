<?php

/* 
 * A project of CuongDCDev@gmail.com
 * */

add_filter( 'cfs_options_screens', function($screens){
    $screens[] = array(
		'name'            => 'shop_brand_option',
		'menu_title'      => __( 'Cấu hình site thương hiệu' , CUONGDC_VENDOR_DOMAIN ),
		'page_title'      => __( 'Cấu hình site thương hiệu' , CUONGDC_VENDOR_DOMAIN ),
		'menu_position'   => 100,
		'icon'            => 'dashicons-admin-multisite', // optional, dashicons-admin-generic is the default
		'field_groups'    => array( 'Thương hiệu' , 
                                             'Quyền đặt quảng cáo' , 'Cấu hình Email'), // Field Group name(s) of CFS Field Group to use on this page (can also be post IDs)
	);

	return $screens;
} 
    
);
