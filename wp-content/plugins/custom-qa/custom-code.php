<?php
/*
Plugin Name: Custom code
Plugin URI: https://webchuanseo.design/
Description: Custom code
Version: 1.0
Author: DEv qa
Author URI: https://webchuanseo.design
*/
require_once( 'include/menu-widget.php' );

function list_video( $args ) {
	//var_dump( $args ); die;
	ob_start();
	?>
    <div class="row">
        <div class="col  large-12">
            <div class="col-inner">
                <div class="row row-custom list-item has-block tooltipstered">
                    <!--START LIST THUONG HIEU-->
                    <h3 class="section-title section-title-normal"><b></b><span class="section-title-main"
                                                                                style="color:rgb(232, 137, 47);"><?php echo $args['tieude'] ?></span><b></b>
                    </h3>
					<?php $videos = new WP_Query( array(
						'post_type'      => 'video',
						'posts_per_page' => 12,
						'category__in'   => array( intval( $args['category_id'] ) ),
						'paged'          => get_query_var( 'paged' ),

					) );

					while ( $videos->have_posts() ):
						$videos->the_post();

						$url = CFS()->get( 'nhap_link_thuong_hieu' );

						?>

                        <div class="col medium-3  small-6 large-2" id="thuonghieu-1">

                            <div class="content-item">
                                <a href="https://www.youtube.com/embed/<?php echo ! empty( get_the_excerpt() ) ? cuongdc_trim_strip( explode( "=", get_the_excerpt() )[1] ) : '#' ?>"
                                   title="<?php echo get_the_title(); ?>"
                                   class="iframe fancybox.iframe">
									<?php echo get_the_post_thumbnail( null, "normal" ); ?>
                                </a>
                                <div class="text-content-item">
                                    <a href="<?php echo ! empty( $url ) ? trim( $url ) : '#' ?>"
                                       title="<?php the_title() ?>">
                                        <h5><?php the_title(); ?></h5>
                                    </a>

                                </div>
                            </div>

                        </div>

					<?php endwhile;
					wp_reset_postdata(); ?>


                    <!-- END LIST THUONG HIEU-->
                </div>

            </div>
        </div>
    </div>
	<?php
	return ob_get_clean();
}

add_shortcode( 'list_video', 'list_video' );

add_shortcode( 'user_name', 'view_user' );
function view_user() {

    global $current_user;
	wp_get_current_user();

	$ht = '
	<ul>
	<li><a href="#"> Xin chào : <span class="display-name">'.$current_user->display_name .'</span></a> </li>
	<li><a id="mail-unread" href="'.get_site_url().'/dashboard/settings/store/#qa_manager'.'"><i class="fa fa-envelope"></i> ('.cuongdc_get_unread_messange().')</a></li>
   </ul>
	
	';
	return $ht;
}