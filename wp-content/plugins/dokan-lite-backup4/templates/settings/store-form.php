<?php
/**
 * Dokan Dashboard Settings Store Form Template
 *
 * @since 2.4
 */
?>
<?php

    $gravatar       = isset( $profile_info['gravatar'] ) ? absint( $profile_info['gravatar'] ) : 0;
    $banner         = isset( $profile_info['banner'] ) ? absint( $profile_info['banner'] ) : 0;
    $storename      = isset( $profile_info['store_name'] ) ? esc_attr( $profile_info['store_name'] ) : '';
    $store_ppp      = isset( $profile_info['store_ppp'] ) ? esc_attr( $profile_info['store_ppp'] ) : '';
    $phone          = isset( $profile_info['phone'] ) ? esc_attr( $profile_info['phone'] ) : '';
    $show_email     = isset( $profile_info['show_email'] ) ? esc_attr( $profile_info['show_email'] ) : 'no';
    $show_more_ptab = isset( $profile_info['show_more_ptab'] ) ? esc_attr( $profile_info['show_more_ptab'] ) : 'yes';
    
   

    $address         = isset( $profile_info['address'] ) ? $profile_info['address'] : '';
    $address_street1 = isset( $profile_info['address']['street_1'] ) ? $profile_info['address']['street_1'] : '';
    $address_street2 = isset( $profile_info['address']['street_2'] ) ? $profile_info['address']['street_2'] : '';
    $address_city    = isset( $profile_info['address']['city'] ) ? $profile_info['address']['city'] : '';
    $address_zip     = isset( $profile_info['address']['zip'] ) ? $profile_info['address']['zip'] : '';
    $address_country = isset( $profile_info['address']['country'] ) ? $profile_info['address']['country'] : '';
    $address_state   = isset( $profile_info['address']['state'] ) ? $profile_info['address']['state'] : '';

    $map_location   = isset( $profile_info['location'] ) ? esc_attr( $profile_info['location'] ) : '';
    $map_address    = isset( $profile_info['find_address'] ) ? esc_attr( $profile_info['find_address'] ) : '';
    $dokan_category = isset( $profile_info['dokan_category'] ) ? $profile_info['dokan_category'] : '';
    $enable_tnc     = isset( $profile_info['enable_tnc'] ) ? $profile_info['enable_tnc'] : '';
    $store_tnc      = isset( $profile_info['store_tnc'] ) ? $profile_info['store_tnc'] : '' ;
    
    
    /**
     * Add them field
     */
     $shop_intro = isset( $profile_info['editor_shop_intro'] ) ? ( $profile_info['editor_shop_intro']  ) : '';
     $shop_guide = isset( $profile_info['editor_shop_guide'] ) ?  $profile_info['editor_shop_guide']  : '';
     $shop_info        = isset( $profile_info['editor_shop_info'] ) ? $profile_info['editor_shop_info'] : '';
     $shop_fb   = isset( $profile_info['shop_fb'] )  ? $profile_info['shop_fb'] : '';
     $shop_twitter   = isset( $profile_info['shop_twitter'] )  ? $profile_info['shop_twitter'] : '';
     $shop_url  = isset( $profile_info['shop_url'] ) ? $profile_info['shop_url'] : '';
     $shop_short_intro = isset( $profile_info['shop_short_intro'] ) ? $profile_info['shop_short_intro'] : '';
     $shop_money_start = isset( $profile_info['shop_money_start'] ) ? $profile_info['shop_money_start'] : '';// chi phi tham gia 
     $shop_cmt = isset( $profile_info['shop_cmt'] ) ? $profile_info['shop_cmt'] : '';// CMT ND
   //  $shop_widget_ads = isset( $profile_info['shop_widget_ads'] ) ? $profile_info['shop_widget_ads'] : '';// Phần quảng cáo 
      
    if ( is_wp_error( $validate ) ) {
        $storename    = $_POST['dokan_store_name']; // WPCS: CSRF ok, Input var ok.
        $map_location = $_POST['location'];         // WPCS: CSRF ok, Input var ok.
        $map_address  = $_POST['find_address'];     // WPCS: CSRF ok, Input var ok.
        
        $posted_address = wc_clean( $_POST['dokan_address'] ); // WPCS: CSRF ok, Input var ok.
        
        $address_street1 = $posted_address['street_1'];
        $address_street2 = $posted_address['street_2'];
        $address_city    = $posted_address['city'];
        $address_zip     = $posted_address['zip'];
        $address_country = $posted_address['country'];
        $address_state   = $posted_address['state'];
    }

    $dokan_appearance = dokan_get_option( 'store_header_template', 'dokan_appearance', 'default' );

//    var_dump( get_user_meta(  $current_user , 'dokan_profile_settings'  ,true  )); die;

?>
<?php do_action( 'dokan_settings_before_form', $current_user, $profile_info ); ?>

    <form method="post" id="store-form"  action="" class="dokan-form-horizontal">
    <div id="tab-settings" >
        <?php wp_nonce_field( 'dokan_store_settings_nonce' ); ?>
<div class="tab" id="edit_brand">
            <div class="dokan-banner">

                <div class="image-wrap<?php echo $banner ? '' : ' dokan-hide'; ?>">
                    <?php $banner_url = $banner ? wp_get_attachment_url( $banner ) : ''; ?>
                    <input type="hidden" class="dokan-file-field" value="<?php echo $banner; ?>" name="dokan_banner">
                    <img class="dokan-banner-img" src="<?php echo esc_url( $banner_url ); ?>">

                    <a class="close dokan-remove-banner-image">&times;</a>
                </div>

                <div class="button-area<?php echo $banner ? ' dokan-hide' : ''; ?>">
                    <i class="fa fa-cloud-upload"></i>

                    <a href="#" class="dokan-banner-drag dokan-btn dokan-btn-info dokan-theme"><?php _e( 'Tải ảnh banner', 'dokan-lite' ); ?></a>
                    <p class="help-block">
                        <?php
                        /**
                         * Filter `dokan_banner_upload_help`
                         *
                         * @since 2.4.10
                         */
                        $general_settings = get_option( 'dokan_general', [] );
                        $banner_width = ! empty( $general_settings['store_banner_width'] ) ? $general_settings['store_banner_width'] : 1175;
                        $banner_height = ! empty( $general_settings['store_banner_height'] ) ? $general_settings['store_banner_height'] : 293;

                        $help_text = sprintf(
                            __('Tải ảnh banner cho trang của bạn, kích cỡ là (%sx%s) px.', 'dokan-lite' ),
                            $banner_width, $banner_height
                        );

                        echo apply_filters( 'dokan_banner_upload_help', $help_text );
                        ?>
                    </p>
                </div>
            </div> <!-- .dokan-banner -->

            <?php do_action( 'dokan_settings_after_banner', $current_user, $profile_info ); ?>

        <div class="dokan-form-group">
            <label class="dokan-w3 dokan-control-label" for="dokan_gravatar"><?php _e( 'Ảnh đại diện thương hiệu', 'dokan-lite' ); ?></label>

            <div class="dokan-w5 dokan-gravatar">
                <div class="dokan-left gravatar-wrap<?php echo $gravatar ? '' : ' dokan-hide'; ?>">
                    <?php $gravatar_url = $gravatar ? wp_get_attachment_url( $gravatar ) : ''; ?>
                    <input type="hidden" class="dokan-file-field" value="<?php echo $gravatar; ?>" name="dokan_gravatar">
                    <img class="dokan-gravatar-img" src="<?php echo esc_url( $gravatar_url ); ?>">
                    <a class="dokan-close dokan-remove-gravatar-image">&times;</a>
                </div>
                <div class="gravatar-button-area<?php echo $gravatar ? ' dokan-hide' : ''; ?>">
                    <a href="#" class="dokan-pro-gravatar-drag dokan-btn dokan-btn-default"><i class="fa fa-cloud-upload"></i> <?php _e( 'Upload Photo', 'dokan-lite' ); ?></a>
                </div>
            </div>
        </div>

        <div class="dokan-form-group">
            <label class="dokan-w3 dokan-control-label" for="dokan_store_name"><?php _e( 'Tên thương hiệu', 'dokan-lite' ); ?></label>

            <div class="dokan-w5 dokan-text-left">
                <input id="dokan_store_name" required value="<?php echo $storename; ?>" name="dokan_store_name" placeholder="<?php _e( 'store name', 'dokan-lite'); ?>" class="dokan-form-control" type="text">
            </div>
        </div>
            
            
        <div class="dokan-form-group">
            <label class="dokan-w3 dokan-control-label" for="dokan_store_name"><?php _e( 'Chi phí tham gia', 'dokan-lite' ); ?></label>

            <div class="dokan-w5 dokan-text-left">
                <input id="dokan_phi_tham_gia"  value="<?php echo $shop_money_start; ?>" name="shop_money_start" 
                       placeholder="<?php _e( 'Nhập chi phí tham gia', 'dokan-lite'); ?>" class="dokan-form-control" type="text">
            </div>
        </div>

        <div class="dokan-form-group" style="display:none">
            <label class="dokan-w3 dokan-control-label" for="dokan_store_ppp"><?php _e( 'Store Product Per Page', 'dokan-lite' ); ?></label>

            <div class="dokan-w5 dokan-text-left">
                <input id="dokan_store_ppp" value="<?php echo $store_ppp; ?>" name="dokan_store_ppp" placeholder="10" class="dokan-form-control" type="number">
            </div>
        </div>
         <!--address-->

        <?php
        $verified = false;

        if ( isset( $profile_info['dokan_verification']['info']['store_address']['v_status'] ) ) {
            if ( $profile_info['dokan_verification']['info']['store_address']['v_status'] == 'approved' ){
                $verified = true;
            }
        }

        dokan_seller_address_fields( $verified );

        ?>
        <!--address-->

        <div class="dokan-form-group">
            <label class="dokan-w3 dokan-control-label" for="setting_phone"><?php _e( 'Số điện thoại', 'dokan-lite' ); ?></label>
            <div class="dokan-w5 dokan-text-left">
                <input id="setting_phone" value="<?php echo $phone; ?>" name="setting_phone" placeholder="<?php _e( '+123456..', 'dokan-lite' ); ?>" class="dokan-form-control input-md" type="text">
            </div>
        </div>
        
        
        <div class="dokan-form-group">
            <label class="dokan-w3 dokan-control-label" for="setting_phone"><?php _e( 'Nhập CMTND ', 'dokan-lite' ); ?></label>
            <div class="dokan-w5 dokan-text-left">
                <input id="setting_cmt" value="<?php echo $shop_cmt; ?>" name="shop_cmt" placeholder="Nhập Chứng minh thư Nhân dân của bạn"
                       class="dokan-form-control input-md" type="text">
            </div>
        </div>
        
        <div class="dokan-form-group">
            <label class="dokan-w3 dokan-control-label" for="shop_url"><?php _e( 'Liên kết ngoài', 'dokan-lite' ); ?></label>
            <div class="dokan-w5 dokan-text-left">
                <input id="shop_url" value="<?php echo $shop_url; ?>" name="shop_url" 
                placeholder="<?php _e( 'địa chỉ website của bạn: http:// ', 'dokan-lite' ); ?>" class="dokan-form-control input-md" type="text" />
            </div>
        </div>

          <div class="dokan-form-group">
            <label class="dokan-w3 dokan-control-label" for="shop_url"><?php _e( 'Thông tin ngắn', 'dokan-lite' ); ?></label>
            <div class="dokan-w5 dokan-text-left">
                <textarea id="shop_short_intro" name="shop_short_intro" 
                placeholder="<?php _e( 'Nhập thông tin giới thiệu ngắn về thương hiệu của bạn, tố đa 100 ký tự', 'dokan-lite' ); ?>"
                class="dokan-form-control input-md" ><?php echo $shop_short_intro; ?></textarea>
                <p class="short_info_counter">Số ký tự: <span id="short_info_counter"></span></p>
            </div>
        </div>



        
        <div class="dokan-form-group shop-intro">
            <label class="dokan-w3 dokan-control-label"><?php _e( 'Nội dung phần giới thiệu thương hiệu', 'dokan-lite' ); ?></label>
            <div class="dokan-w5 dokan-text-left">
                <div class="dokan-form-control">
                    <?php wp_editor($shop_intro , 'editor_shop_intro' , array(
                        'wpautop' => true, // use wpautop?
                        'media_buttons' => true, // show insert/upload button(s)
                        'textarea_name' => 'editor_shop_intro', // set the textarea name to something different, square brackets [] can be used here
                        'tabindex' => '',
                        'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
                        'editor_class' => '', // add extra class(es) to the editor textarea
                        'teeny' => false, // output the minimal editor config used in Press This
                        'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
                        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
                    )) ?>
                </div>
            </div>
        </div>

   
        <div class="dokan-form-group shop-intro">
            <label class="dokan-w3 dokan-control-label"><?php _e( 'Nội dung phần hướng dẫn thương hiệu ', 'dokan-lite' ); ?></label>
            <div class="dokan-w5 dokan-text-left">
                <div class="dokan-form-control">
                    <?php wp_editor($shop_guide , 'editor_shop_guide' , array(
                        'wpautop' => true, // use wpautop?
                        'media_buttons' => true, // show insert/upload button(s)
                        'textarea_name' => 'editor_shop_guide', // set the textarea name to something different, square brackets [] can be used here
                        'tabindex' => '',
                        'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
                        'editor_class' => '', // add extra class(es) to the editor textarea
                        'teeny' => false, // output the minimal editor config used in Press This
                        'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
                        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
                    )) ?>
                </div>
            </div>
        </div>


   
        <div class="dokan-form-group shop-intro">
            <label class="dokan-w3 dokan-control-label"><?php _e( 'Nội dung phần thông tin ngành thương hiệu', 'dokan-lite' ); ?></label>
            <div class="dokan-w5 dokan-text-left">
                <div class="dokan-form-control">
                    <?php wp_editor($shop_info , 'editor_shop_info' , array(
                        'wpautop' => true, // use wpautop?
                        'media_buttons' => true, // show insert/upload button(s)
                        'textarea_name' => 'editor_shop_info', // set the textarea name to something different, square brackets [] can be used here
                        'tabindex' => '',
                        'editor_css' => '', //  extra styles for both visual and HTML editors buttons, 
                        'editor_class' => '', // add extra class(es) to the editor textarea
                        'teeny' => false, // output the minimal editor config used in Press This
                        'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)
                        'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()
                        'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()
                    )) ?>
                </div>
            </div>
        </div>
    </div><!-- .edit_brand -->

   
    
<div class="tab" id="edit_left_widget">
       <div class="dokan-form-group">
           <input type="hidden" name="shop_widget_orders" id="shop_widget_orders" />


           <div class="dokan-form-group">
               <label class="dokan-w3 dokan-control-label" for="setting_map"><?php _e( 'Link fanpage Facebook', 'dokan-lite' ); ?></label>

               <div class="dokan-w6 dokan-text-left">
                           <input name="shop_fb" id="shop_fb" type="text" class="dokan-form-control input-md" value="<?php echo $shop_fb; ?>"
                                  placeholder="<?php _e( 'Nhập địa chỉ Facebook: https://facebook.com/trang_cua_ban', 'dokan-lite' ); ?>" size="30" />
               </div> <!-- col.md-4 -->
           </div> <!-- .dokan-form-group -->


           <div class="dokan-form-group">
               <label class="dokan-w3 dokan-control-label" for="setting_map"><?php _e( 'Link fanpage Twitter', 'dokan-lite' ); ?></label>

               <div class="dokan-w6 dokan-text-left">
                           <input name="shop_twitter" id="shop_twitter" type="text" class="dokan-form-control input-md" value="<?php echo $shop_twitter; ?>"
                                  placeholder="<?php _e( 'Nhập địa chỉ Twitter: https://twitter.com/trang_cua_ban', 'dokan-lite' ); ?>" size="30" />
               </div> <!-- col.md-4 -->
           </div> <!-- .dokan-form-group -->
           
           
       <!--<div class="dokan-form-group form-quang-cao">-->
           <!--<label class="dokan-w3 dokan-control-label" for="setting_map"><?php // _e( 'Quảng cáo', 'dokan-lite' ); ?></label>-->

       <!-- <div class="dokan-w6 dokan-text-left">-->
       <!--        <div class="dokan-form-control">-->
       <!--             <?php /** wp_editor($shop_widget_ads , 'shop_widget_ads' , array(-->
       <!--                 'wpautop' => true, // use wpautop?-->
       <!--                 'media_buttons' => true, // show insert/upload button(s)-->
       <!--                 'textarea_name' => 'shop_widget_ads', // set the textarea name to something different, square brackets [] can be used here-->
       <!--                 'tabindex' => '',-->
       <!--                 'editor_css' => '', //  extra styles for both visual and HTML editors buttons, -->
       <!--                 'editor_class' => '', // add extra class(es) to the editor textarea-->
       <!--                 'teeny' => false, // output the minimal editor config used in Press This-->
       <!--                 'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)-->
       <!--                 'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()-->
       <!--                 'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()-->
       <!--             )) **/ ?>-->
       <!--        </div> -->
       <!-- </div> <!-- col.md-4 -->
           
       </div> <!-- .dokan-form-group , FORM QUANG CAO -->
       
       

       <div class="dokan-form-group">
           <label class="dokan-w3 dokan-control-label" for="setting_map"><?php _e( 'Địa chỉ', 'dokan-lite' ); ?></label>

           <div class="dokan-w6 dokan-text-left">
               <input id="dokan-map-lat" type="hidden" name="location" value="<?php echo $map_location; ?>" size="30" />

               <div class="dokan-map-wrap">
                   <div class="dokan-map-search-bar">
                       <input id="dokan-map-add" type="text" class="dokan-map-search" value="<?php echo $map_address; ?>" name="find_address" placeholder="<?php _e( 'Type an address to find', 'dokan-lite' ); ?>" size="30" />
                       <a href="#" class="dokan-map-find-btn" id="dokan-location-find-btn" type="button"><?php _e( 'Tìm địa chỉ', 'dokan-lite' ); ?></a>
                   </div>

                   <div class="dokan-google-map" id="dokan-map"></div>
               </div>
           </div> <!-- col.md-4 -->
       </div> <!-- .dokan-form-group -->

    <!--<label class="dokan-w3 dokan-control-label" for="setting_map"><?php //_e( 'Vị trí xuất hiện', 'dokan-lite' ); ?></label>-->
       <!--<div class="dokan-w6 dokan-text-left">   -->
       <!--  <div id="widget-order">-->
       <!--    <ul class="widget-order-wrap">-->
       <!--        <li data-name="fanpage">Fanpage</li>-->
       <!--        <li data-name="map">Bản đồ</li>-->
       <!--         <li data-name="ads">Quảng cáo</li>-->
       <!--    </ul>-->
       <!--  </div><!-- widget-order-->
       
       <!--  <div class="hint">-->
       <!--     Vị trí xuất hiện của widget bên phải trang thương hiệu của bạn. Kéo thả để đổi vị trí-->
       <!--  </div>-->
       <!--</div>-->
       <!--</div>-->
       
   </div><!-- edit_left_widget -->

   
   <div class="tab" id="qa_manager">
       <div class="dokan-form-group">
           <iframe class="manage-inbox" style="width:100%;height:1024px;" src="<?php echo get_admin_url()?>/edit.php?s&post_status=all&post_type=wpcf7s" >
               
           </iframe>
       </div>
   </div><!-- #qa_manager -->
   
   
        <!--terms and conditions enable or not -->
        <?php
        $tnc_enable = dokan_get_option( 'seller_enable_terms_and_conditions', 'dokan_general', 'off' );
        if ( $tnc_enable == 'on' ) :
            ?>
            <div class="dokan-form-group">
                <label class="dokan-w3 dokan-control-label"><?php _e( 'Terms and Conditions', 'dokan-lite' ); ?></label>
                <div class="dokan-w5 dokan-text-left dokan_tock_check">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" id="dokan_store_tnc_enable" value="on" <?php echo $enable_tnc == 'on' ? 'checked':'' ; ?> name="dokan_store_tnc_enable" ><?php _e( 'Show terms and conditions in store page', 'dokan-lite' ); ?>
                        </label>
                    </div>
                </div>
            </div>
            <div class="dokan-form-group" id="dokan_tnc_text">
                <label class="dokan-w3 dokan-control-label" for="dokan_store_tnc"><?php _e( 'TOC Details', 'dokan-lite' ); ?></label>
                <div class="dokan-w8 dokan-text-left">
                    <?php
                        $settings = array(
                            'editor_height' => 200,
                            'media_buttons' => false,
                            'teeny'         => true,
                            'quicktags'     => false
                        );
                        wp_editor( $store_tnc, 'dokan_store_tnc', $settings );
                    ?>
                </div>
            </div>

        <?php endif;?>


        <?php do_action( 'dokan_settings_form_bottom', $current_user, $profile_info ); ?>

        <div class="dokan-form-group">

            <div class="dokan-w4 ajax_prev dokan-text-left" style="margin-left:24%;">
                <input type="submit" name="dokan_update_store_settings" class="dokan-btn dokan-btn-danger dokan-btn-theme" value="<?php esc_attr_e( 'Update Settings', 'dokan-lite' ); ?>">
            </div>
        </div>
    </div><!-- #tab-settings -->
    </form>

    <?php do_action( 'dokan_settings_after_form', $current_user, $profile_info ); ?>

<style>
    .dokan-settings-content .dokan-settings-area .dokan-banner {
        width: <?php echo $banner_width . 'px'; ?>;
        height: <?php echo $banner_height . 'px'; ?>;
    }

    .dokan-settings-content .dokan-settings-area .dokan-banner .dokan-remove-banner-image {
        height: <?php echo $banner_height . 'px'; ?>;
    }
</style>
<script type="text/javascript">

    (function($) {
        var dokan_address_wrapper = $( '.dokan-address-fields' );
        var dokan_address_select = {
            init: function () {

                dokan_address_wrapper.on( 'change', 'select.country_to_state', this.state_select );
            },
            state_select: function () {
                var states_json = wc_country_select_params.countries.replace( /&quot;/g, '"' ),
                    states = $.parseJSON( states_json ),
                    $statebox = $( '#dokan_address_state' ),
                    input_name = $statebox.attr( 'name' ),
                    input_id = $statebox.attr( 'id' ),
                    input_class = $statebox.attr( 'class' ),
                    value = $statebox.val(),
                    selected_state = '<?php echo $address_state; ?>',
                    input_selected_state = '<?php echo $address_state; ?>',
                    country = $( this ).val();

                if ( states[ country ] ) {

                    if ( $.isEmptyObject( states[ country ] ) ) {

                        $( 'div#dokan-states-box' ).slideUp( 2 );
                        if ( $statebox.is( 'select' ) ) {
                            $( 'select#dokan_address_state' ).replaceWith( '<input type="text" class="' + input_class + '" name="' + input_name + '" id="' + input_id + '" required />' );
                        }

                        $( '#dokan_address_state' ).val( 'N/A' );

                    } else {
                        input_selected_state = '';

                        var options = '',
                            state = states[ country ];

                        for ( var index in state ) {
                            if ( state.hasOwnProperty( index ) ) {
                                if ( selected_state ) {
                                    if ( selected_state == index ) {
                                        var selected_value = 'selected="selected"';
                                    } else {
                                        var selected_value = '';
                                    }
                                }
                                options = options + '<option value="' + index + '"' + selected_value + '>' + state[ index ] + '</option>';
                            }
                        }

                        if ( $statebox.is( 'select' ) ) {
                            $( 'select#dokan_address_state' ).html( '<option value="">' + wc_country_select_params.i18n_select_state_text + '</option>' + options );
                        }
                        if ( $statebox.is( 'input' ) ) {
                            $( 'input#dokan_address_state' ).replaceWith( '<select type="text" class="' + input_class + '" name="' + input_name + '" id="' + input_id + '" required ></select>' );
                            $( 'select#dokan_address_state' ).html( '<option value="">' + wc_country_select_params.i18n_select_state_text + '</option>' + options );
                        }
                        $( '#dokan_address_state' ).removeClass( 'dokan-hide' );
                        $( 'div#dokan-states-box' ).slideDown();

                    }
                } else {


                    if ( $statebox.is( 'select' ) ) {
                        input_selected_state = '';
                        $( 'select#dokan_address_state' ).replaceWith( '<input type="text" class="' + input_class + '" name="' + input_name + '" id="' + input_id + '" required="required"/>' );
                    }
                    $( '#dokan_address_state' ).val(input_selected_state);

                    if ( $( '#dokan_address_state' ).val() == 'N/A' ){
                        $( '#dokan_address_state' ).val('');
                    }
                    $( '#dokan_address_state' ).removeClass( 'dokan-hide' );
                    $( 'div#dokan-states-box' ).slideDown();
                }
            }
        }

        $(function() {
            dokan_address_select.init();

            $('#setting_phone').keydown(function(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 91, 107, 109, 110, 187, 189, 190]) !== -1 ||
                     // Allow: Ctrl+A
                    (e.keyCode == 65 && e.ctrlKey === true) ||
                     // Allow: home, end, left, right
                    (e.keyCode >= 35 && e.keyCode <= 39)) {
                         // let it happen, don't do anything
                    return;
                }

                // Ensure that it is a number and stop the keypress
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            <?php
            $locations = explode( ',', $map_location );
            $def_lat = isset( $locations[0] ) ? $locations[0] : 90.40714300000002;
            $def_long = isset( $locations[1] ) ? $locations[1] : 23.709921;
            ?>
            var def_zoomval = 12;
            var def_longval = '<?php echo $def_long; ?>';
            var def_latval = '<?php echo $def_lat; ?>';

            try {
                var curpoint = new google.maps.LatLng(def_latval, def_longval),
                    geocoder   = new window.google.maps.Geocoder(),
                    $map_area = $('#dokan-map'),
                    $input_area = $( '#dokan-map-lat' ),
                    $input_add = $( '#dokan-map-add' ),
                    $find_btn = $( '#dokan-location-find-btn' );

                $find_btn.on('click', function(e) {
                    e.preventDefault();

                    geocodeAddress( $input_add.val() );
                });

                var gmap = new google.maps.Map( $map_area[0], {
                    center: curpoint,
                    zoom: def_zoomval,
                    mapTypeId: window.google.maps.MapTypeId.ROADMAP
                });

                var marker = new window.google.maps.Marker({
                    position: curpoint,
                    map: gmap,
                    draggable: true
                });

                window.google.maps.event.addListener( gmap, 'click', function ( event ) {
                    marker.setPosition( event.latLng );
                    updatePositionInput( event.latLng );
                } );

                window.google.maps.event.addListener( marker, 'drag', function ( event ) {
                    updatePositionInput(event.latLng );
                } );

            } catch( e ) {
                console.log( 'Google API not found.' );
            }

            autoCompleteAddress();

            function updatePositionInput( latLng ) {
                $input_area.val( latLng.lat() + ',' + latLng.lng() );
            }

            function updatePositionMarker() {
                var coord = $input_area.val(),
                    pos, zoom;

                if ( coord ) {
                    pos = coord.split( ',' );
                    marker.setPosition( new window.google.maps.LatLng( pos[0], pos[1] ) );

                    zoom = pos.length > 2 ? parseInt( pos[2], 10 ) : 12;

                    gmap.setCenter( marker.position );
                    gmap.setZoom( zoom );
                }
            }

            function geocodeAddress( address ) {
                geocoder.geocode( {'address': address}, function ( results, status ) {
                    if ( status == window.google.maps.GeocoderStatus.OK ) {
                        updatePositionInput( results[0].geometry.location );
                        marker.setPosition( results[0].geometry.location );
                        gmap.setCenter( marker.position );
                        gmap.setZoom( 15 );
                    }
                } );
            }

            function autoCompleteAddress(){
                if (!$input_add) return null;

                $input_add.autocomplete({
                    source: function(request, response) {
                        // TODO: add 'region' option, to help bias geocoder.
                        geocoder.geocode( {'address': request.term }, function(results, status) {
                            response(jQuery.map(results, function(item) {
                                return {
                                    label     : item.formatted_address,
                                    value     : item.formatted_address,
                                    latitude  : item.geometry.location.lat(),
                                    longitude : item.geometry.location.lng()
                                };
                            }));
                        });
                    },
                    select: function(event, ui) {

                        $input_area.val(ui.item.latitude + ',' + ui.item.longitude );

                        var location = new window.google.maps.LatLng(ui.item.latitude, ui.item.longitude);

                        gmap.setCenter(location);
                        // Drop the Marker
                        setTimeout( function(){
                            marker.setValues({
                                position    : location,
                                animation   : window.google.maps.Animation.DROP
                            });
                        }, 1500);
                    }
                });
            }

            //order 
//            sortable(".widget-order-wrap");
            
            // sortable(".widget-order-wrap")[0].addEventListener('sortstop' , function(e){
            //      var jsonsort = [];
            //      sortable('.widget-order-wrap', 'serialize')[0].children.forEach( function(data){
            //             jsonsort.push(data.getAttribute("data-name"));
            //     } );
            //         $("#shop_widget_orders").attr( "value", JSON.stringify(jsonsort) );
            //         console.log( $("#shop_widget_orders").attr("value") );
            // });
            
       
       
            $(document).ready(function(){
                //an / hien link tab 
                var anchorId = window.location.hash.substr(1);
                switch( anchorId ){
                    case "edit_brand":
                    showOnlyTab(anchorId);
                    break;

                    case "edit_left_widget":
                    showOnlyTab(anchorId);
                    break;
                    
                    case "qa_manager":
                    showOnlyTab(anchorId);
                    break;
                    
                }//switch

                function showOnlyTab(id){
                $(".tab").hide();
                $("#" + id).show();
                  $(".dokan-dashboard-menu li").each(function(){
                      $(this).removeClass("active");
                  });
                  $(".dokan-dashboard-menu ." + id).addClass("active");
                }
            });//dom ready! 
           
            
        });
    })(jQuery);
</script>


<style>
    body.dragging, body.dragging * {
  cursor: move !important;
}

.dragged {
  position: absolute;
  opacity: 0.5;
  z-index: 2000;
}

ol.example li.placeholder {
  position: relative;
  /** More li styles **/
}
ol.example li.placeholder:before {
  position: absolute;
  /** Define arrowhead **/
}

.widget-order-wrap li{
    background:#E8892F;
    color:black;
    margin-top:5px !important;
}
.ui-sortable-helper{
    background:#E8892F !important;
}
.widget-order-wrap li:hover, .widget-order-wrap li:active{
    cursor:pointer;
}

.sortable-placeholder{
 border:1px solid black;   
}

#edit_brand{
  display: block;
}

.tab{
  display: none;
  
}

#edit_brand .dokan-w5{
    width: 73.667%;
}

</style>
<script>
    //JQUERY SORTABLE 
!function(e,t){"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?module.exports=t():e.sortable=t()}(this,function(){"use strict";function e(e,t,n){var r=null;return 0===t?e:function(){var a=n||this,o=arguments;clearTimeout(r),r=setTimeout(function(){e.apply(a,o)},t)}}var t,n,r=[],a=[],o=function(e,t,n){return void 0===n?e&&e.h5s&&e.h5s.data&&e.h5s.data[t]:(e.h5s=e.h5s||{},e.h5s.data=e.h5s.data||{},e.h5s.data[t]=n,void 0)},i=function(e){e.h5s&&delete e.h5s.data},s=function(e,t){return(e.matches||e.matchesSelector||e.msMatchesSelector||e.mozMatchesSelector||e.webkitMatchesSelector||e.oMatchesSelector).call(e,t)},l=function(e,t){if(!t)return Array.prototype.slice.call(e);for(var n=[],r=0;r<e.length;++r)"string"==typeof t&&s(e[r],t)&&n.push(e[r]),t.indexOf(e[r])!==-1&&n.push(e[r]);return n},d=function(e,t,n){if(e instanceof Array)for(var r=0;r<e.length;++r)d(e[r],t,n);else e.addEventListener(t,n),e.h5s=e.h5s||{},e.h5s.events=e.h5s.events||{},e.h5s.events[t]=n},c=function(e,t){if(e instanceof Array)for(var n=0;n<e.length;++n)c(e[n],t);else e.h5s&&e.h5s.events&&e.h5s.events[t]&&(e.removeEventListener(t,e.h5s.events[t]),delete e.h5s.events[t])},f=function(e,t,n){if(e instanceof Array)for(var r=0;r<e.length;++r)f(e[r],t,n);else e.setAttribute(t,n)},u=function(e,t){if(e instanceof Array)for(var n=0;n<e.length;++n)u(e[n],t);else e.removeAttribute(t)},p=function(e){var t=e.getClientRects()[0];return{left:t.left+window.scrollX,top:t.top+window.scrollY}},h=function(e){c(e,"dragstart"),c(e,"dragend"),c(e,"selectstart"),c(e,"dragover"),c(e,"dragenter"),c(e,"drop")},g=function(e){c(e,"dragover"),c(e,"dragenter"),c(e,"drop")},m=function(e,t){e.dataTransfer.effectAllowed="move",e.dataTransfer.setData("text","arbitrary-content"),e.dataTransfer.setDragImage&&e.dataTransfer.setDragImage(t.draggedItem,t.x,t.y)},v=function(e,t){return t.x||(t.x=parseInt(e.pageX-p(t.draggedItem).left)),t.y||(t.y=parseInt(e.pageY-p(t.draggedItem).top)),t},y=function(e){return{draggedItem:e}},b=function(e,t){var n=y(t);n=v(e,n),m(e,n)},E=function(e){i(e),u(e,"aria-dropeffect")},x=function(e){u(e,"aria-grabbed"),u(e,"draggable"),u(e,"role")},w=function(e,t){return e===t||void 0!==o(e,"connectWith")&&o(e,"connectWith")===o(t,"connectWith")},I=function(e,t){var n,r=[];if(!t)return e;for(var a=0;a<e.length;++a)n=e[a].querySelectorAll(t),r=r.concat(Array.prototype.slice.call(n));return r},C=function(e){var t=o(e,"opts")||{},n=l(z(e),t.items),r=I(n,t.handle);g(e),E(e),c(r,"mousedown"),h(n),x(n)},A=function(e){var t=o(e,"opts"),n=l(z(e),t.items),r=I(n,t.handle);f(e,"aria-dropeffect","move"),o(e,"_disabled","false"),f(r,"draggable","true");var a=(document||window.document).createElement("span");"function"!=typeof a.dragDrop||t.disableIEFix||d(r,"mousedown",function(){if(n.indexOf(this)!==-1)this.dragDrop();else{for(var e=this.parentElement;n.indexOf(e)===-1;)e=e.parentElement;e.dragDrop()}})},S=function(e){var t=o(e,"opts"),n=l(z(e),t.items),r=I(n,t.handle);f(e,"aria-dropeffect","none"),o(e,"_disabled","true"),f(r,"draggable","false"),c(r,"mousedown")},D=function(e){var t=o(e,"opts"),n=l(z(e),t.items),r=I(n,t.handle);o(e,"_disabled","false"),h(n),c(r,"mousedown"),g(e)},L=function(e){return e.parentElement?Array.prototype.indexOf.call(e.parentElement.children,e):0},O=function(e){return!!e.parentNode},T=function(e,t){if("string"!=typeof e)return e;var n=document.createElement(t);return n.innerHTML=e,n.firstChild},W=function(e,t){e.parentElement.insertBefore(t,e)},M=function(e,t){e.parentElement.insertBefore(t,e.nextElementSibling)},N=function(e){e.parentNode&&e.parentNode.removeChild(e)},P=function(e,t){var n=document.createEvent("Event");return t&&(n.detail=t),n.initEvent(e,!1,!0),n},_=function(e,t){a.forEach(function(n){w(e,n)&&n.dispatchEvent(t)})},z=function(e){return e.children},Y=function(e){var t=l(z(e),o(e,"items"));return t},q=function(i,c){var u=String(c);if(c=function(e){var t={connectWith:!1,placeholder:null,disableIEFix:!1,placeholderClass:"sortable-placeholder",draggingClass:"sortable-dragging",hoverClass:!1,debounce:0,maxItems:0};for(var n in e)t[n]=e[n];return t}(c),c&&"function"==typeof c.getChildren&&(z=c.getChildren),"string"==typeof i&&(i=document.querySelectorAll(i)),i instanceof window.Element&&(i=[i]),i=Array.prototype.slice.call(i),/serialize/.test(u)){var h=[];return i.forEach(function(e){h.push({list:e,children:Y(e)})}),h}return i.forEach(function(i){if(/enable|disable|destroy/.test(u))return q[u](i);c=o(i,"opts")||c,o(i,"opts",c),D(i);var h,g,m,v,y=l(z(i),c.items),E=c.placeholder;if(E||(E=document.createElement(/^ul|ol$/i.test(i.tagName)?"li":"div")),E=T(E,i.tagName),E.classList.add.apply(E.classList,c.placeholderClass.split(" ")),!i.getAttribute("data-sortable-id")){var x=a.length;a[x]=i,f(i,"data-sortable-id",x),f(y,"data-item-sortable-id",x)}if(o(i,"items",c.items),r.push(E),c.connectWith&&o(i,"connectWith",c.connectWith),A(i),f(y,"role","option"),f(y,"aria-grabbed","false"),c.hoverClass){var I="sortable-over";"string"==typeof c.hoverClass&&(I=c.hoverClass),d(y,"mouseenter",function(){this.classList.add(I)}),d(y,"mouseleave",function(){this.classList.remove(I)})}c.maxItems&&"number"==typeof c.maxItems&&(v=c.maxItems),d(y,"dragstart",function(e){e.stopImmediatePropagation(),c.handle&&!s(e.target,c.handle)||"false"===this.getAttribute("draggable")||(b(e,this),this.classList.add(c.draggingClass),t=this,f(t,"aria-grabbed","true"),h=L(t),n=parseInt(window.getComputedStyle(t).height),g=this.parentElement,m=Y(g),_(i,P("sortstart",{item:t,placeholder:E,startparent:g})))}),d(y,"dragend",function(){var e;t&&(t.classList.remove(c.draggingClass),f(t,"aria-grabbed","false"),t.style.display=t.oldDisplay,delete t.oldDisplay,r.forEach(N),e=this.parentElement,_(i,P("sortstop",{item:t,startparent:g})),h===L(t)&&g===e||_(i,P("sortupdate",{item:t,index:l(z(e),o(e,"items")).indexOf(t),oldindex:y.indexOf(t),elementIndex:L(t),oldElementIndex:h,startparent:g,endparent:e,newEndList:Y(e),newStartList:Y(g),oldStartList:m})),t=null,n=null)}),d([i,E],"drop",function(e){var n;w(i,t.parentElement)&&(e.preventDefault(),e.stopPropagation(),n=r.filter(O)[0],M(n,t),t.dispatchEvent(P("dragend")))});var C=e(function(e,a){if(t)if(y.indexOf(e)!==-1){var o=parseInt(window.getComputedStyle(e).height),i=L(E),s=L(e);if(c.forcePlaceholderSize&&(E.style.height=n+"px"),o>n){var d=o-n,f=p(e).top;if(i<s&&a<f+d)return;if(i>s&&a>f+o-d)return}void 0===t.oldDisplay&&(t.oldDisplay=t.style.display),"none"!==t.style.display&&(t.style.display="none"),i<s?M(e,E):W(e,E),r.filter(function(e){return e!==E}).forEach(N)}else r.indexOf(e)!==-1||l(z(e),c.items).length||(r.forEach(N),e.appendChild(E))},c.debounce),S=function(e){t&&w(i,t.parentElement)&&"true"!==o(i,"_disabled")&&(v&&l(z(i),o(i,"items")).length>=v||(e.preventDefault(),e.stopPropagation(),e.dataTransfer.dropEffect="move",C(this,e.pageY)))};d(y.concat(i),"dragover",S),d(y.concat(i),"dragenter",S)}),i};return q.destroy=function(e){C(e)},q.enable=function(e){A(e)},q.disable=function(e){S(e)},q});

    (function($){
        $(document).ready(function(){
        
            $("#short_info_counter").html( $("#shop_short_intro").val().trim().length  +" / 100 ");
            
            $("#shop_short_intro").on("keyup" , function(){
                $("#short_info_counter").html( $("#shop_short_intro").val().trim().length  +" / 100 ");
            });
        });//dom ready 
        
    })(jQuery);
</script>
