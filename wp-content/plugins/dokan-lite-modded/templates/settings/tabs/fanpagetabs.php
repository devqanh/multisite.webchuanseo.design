<div class="tab" id="edit_left_widget">
       <div class="dokan-form-group">
           <input type="hidden" name="shop_widget_orders" id="shop_widget_orders" />


           <div class="dokan-form-group">
               <label class="dokan-w3 dokan-control-label" for="setting_map"><?php _e( 'Link fanpage Facebook', 'dokan-lite' ); ?></label>

               <div class="dokan-w6 dokan-text-left">
                           <input name="shop_fb" id="shop_fb" type="text" class="dokan-form-control input-md" value="<?php echo $shop_fb; ?>"
                                  placeholder="<?php _e( 'Nhập địa chỉ Facebook: https://facebook.com/trang_cua_ban', 'dokan-lite' ); ?>" size="30" />
               </div> <!-- col.md-4 -->
           </div> <!-- .dokan-form-group -->


           <div class="dokan-form-group">
               <label class="dokan-w3 dokan-control-label" for="setting_map"><?php _e( 'Link fanpage Twitter', 'dokan-lite' ); ?></label>

               <div class="dokan-w6 dokan-text-left">
                           <input name="shop_twitter" id="shop_twitter" type="text" class="dokan-form-control input-md" value="<?php echo $shop_twitter; ?>"
                                  placeholder="<?php _e( 'Nhập địa chỉ Twitter: https://twitter.com/trang_cua_ban', 'dokan-lite' ); ?>" size="30" />
               </div> <!-- col.md-4 -->
           </div> <!-- .dokan-form-group -->
           
           
       <!--<div class="dokan-form-group form-quang-cao">-->
           <!--<label class="dokan-w3 dokan-control-label" for="setting_map"><?php // _e( 'Quảng cáo', 'dokan-lite' ); ?></label>-->

       <!-- <div class="dokan-w6 dokan-text-left">-->
       <!--        <div class="dokan-form-control">-->
       <!--             <?php /** wp_editor($shop_widget_ads , 'shop_widget_ads' , array(-->
       <!--                 'wpautop' => true, // use wpautop?-->
       <!--                 'media_buttons' => true, // show insert/upload button(s)-->
       <!--                 'textarea_name' => 'shop_widget_ads', // set the textarea name to something different, square brackets [] can be used here-->
       <!--                 'tabindex' => '',-->
       <!--                 'editor_css' => '', //  extra styles for both visual and HTML editors buttons, -->
       <!--                 'editor_class' => '', // add extra class(es) to the editor textarea-->
       <!--                 'teeny' => false, // output the minimal editor config used in Press This-->
       <!--                 'dfw' => false, // replace the default fullscreen with DFW (supported on the front-end in WordPress 3.4)-->
       <!--                 'tinymce' => true, // load TinyMCE, can be used to pass settings directly to TinyMCE using an array()-->
       <!--                 'quicktags' => true // load Quicktags, can be used to pass settings directly to Quicktags using an array()-->
       <!--             )) **/ ?>-->
       <!--        </div> -->
       <!-- </div> <!-- col.md-4 -->
           
       </div> <!-- .dokan-form-group , FORM QUANG CAO -->
       
       

       <div class="dokan-form-group">
           <label class="dokan-w3 dokan-control-label" for="setting_map"><?php _e( 'Địa chỉ', 'dokan-lite' ); ?></label>

           <div class="dokan-w6 dokan-text-left">
               <input id="dokan-map-lat" type="hidden" name="location" value="<?php echo $map_location; ?>" size="30" />

               <div class="dokan-map-wrap">
                   <div class="dokan-map-search-bar">
                       <input id="dokan-map-add" type="text" class="dokan-map-search" value="<?php echo $map_address; ?>" name="find_address" placeholder="<?php _e( 'Type an address to find', 'dokan-lite' ); ?>" size="30" />
                       <a href="#" class="dokan-map-find-btn" id="dokan-location-find-btn" type="button"><?php _e( 'Tìm địa chỉ', 'dokan-lite' ); ?></a>
                   </div>

                   <div class="dokan-google-map" id="dokan-map"></div>
               </div>
           </div> <!-- col.md-4 -->
       </div> <!-- .dokan-form-group -->

    <!--<label class="dokan-w3 dokan-control-label" for="setting_map"><?php //_e( 'Vị trí xuất hiện', 'dokan-lite' ); ?></label>-->
       <!--<div class="dokan-w6 dokan-text-left">   -->
       <!--  <div id="widget-order">-->
       <!--    <ul class="widget-order-wrap">-->
       <!--        <li data-name="fanpage">Fanpage</li>-->
       <!--        <li data-name="map">Bản đồ</li>-->
       <!--         <li data-name="ads">Quảng cáo</li>-->
       <!--    </ul>-->
       <!--  </div><!-- widget-order-->
       
       <!--  <div class="hint">-->
       <!--     Vị trí xuất hiện của widget bên phải trang thương hiệu của bạn. Kéo thả để đổi vị trí-->
       <!--  </div>-->
       <!--</div>-->
       <!--</div>-->
       
   </div><!-- edit_left_widget -->