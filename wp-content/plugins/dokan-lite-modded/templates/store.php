<?php
/**
 * Store page cho theme nay
 */

get_header();
do_action( 'flatsome_before_page' );


$user_id   = ( get_query_var( 'author' , false) );
$shop_info   = get_user_meta( $user_id, 'dokan_profile_settings' , true  );
$shop_name   = strip_tags(get_user_meta( $user_id, 'dokan_store_name' , true ));
$titles = array( 
  "Thông tin tổng quan" ,  "Giới thiệu thương hiệu" , "Hướng dẫn tham gia" ,  "Thông tin hãng","Hỏi đáp trực tuyến"
);
$contens = array(
                          "editor_shop_intro"       ,  "editor_shop_guide"  , "editor_shop_info" 
);
$plugin_dir_path = plugin_dir_url( __FILE__ );

 ?>

<div id="content-child" role="main" class="content-area">
    <div class="nav-brand-name" style="background-color: #974F15; ">
        <div class="container">
           <div class="row">
            <div class="col large-10">
                <div class="logo-brand-name">
                    <?php  echo wp_get_attachment_image( $shop_info['gravatar'] , 'normal' ) ?>
                </div>
                <div class="brand-name">
                    <ul>
                        <li><?php echo( cuongdc_trim_strip( $shop_info['store_name'] )); ?></li>
                        <?php if(!empty($shop_info['phone'])):?>
                            <li>Chi phí tham gia：<?php echo !empty($shop_info['shop_money_start']) ? $shop_info['shop_money_start'] : 'Liên hệ'  ?></li>
                        <?php endif;?>
                    </ul>
                </div>
            </div>
               <div class="col large-2 button-brand-name">
                   <a  class="yeu-cau-thong-tin" href="#"><button>Nhận tư vấn</button></a>
               </div>
           </div>
        </div>
    </div>
    <div class="container">
        <div class="row brearcrumb">
            <?php get_flatsome_breadcrumbs(); ?>
            <div class="nav-right-menu-mxh">
                <ul>
                    <li>
                       <a id="add-bookmark" data-uid="<?php echo $user_id ; ?>"  title="Thêm vào yêu thích">
                            <i class="fa <?php echo cuongdc_is_current_user_like_vendor( $user_id ) ? 'fa-star' : 'fa-star-o'?>" aria-hidden="true"></i>
                       </a>
                    </li>
                    <li><a href="<?php echo !empty($shop_info['shop_fb']) ? strip_tags($shop_info['shop_fb']) : "#"?>" title="Fanpage"><img src="<?php echo $plugin_dir_path; ?>img/facebook01.gif" /></a></li>
                    <li><a href="<?php echo !empty($shop_info['shop_twitter']) ? strip_tags($shop_info['shop_twitter']) : "#" ?>" title="Twitter"><img src="<?php echo $plugin_dir_path; ?>img/twitter01.gif" /></a></li>

                </ul>
            </div>
        </div>
        <div class="row img-cover">
            <div class="col large-12">
                <!--<img src="https://multisite.webchuanseo.design/wp-content/uploads/2017/12/20171123_7af5c.jpg">-->
                <?php  echo wp_get_attachment_image($shop_info['banner'] , 'full'  ) ?>
            </div>
        </div>
        <div class="row middle">
            <div class="col large-3">
                <div class=" img-profile">
                    <!--<img src="https://multisite.webchuanseo.design/wp-content/uploads/2017/12/20171123_025b0.jpg" />-->
                    <img src="<?php echo wp_get_attachment_image_url( $shop_info['gravatar'], 'normal' )?>" title="<?php echo $shop_name?>"/>
                </div>
            </div>
            <div class="col large-9 text-profile">
                <h2 class="intro_in_0001 shop-name" ><?php echo $shop_name ?></h2>
                <p class="shop-short-desciption"><?php echo strip_tags( $shop_info['shop_short_intro'] ); ?></p>
            </div>
        </div>

        <div class="row bottom-profile" >
            <div class="col large-9 left-bottom-profile">
                <div class="info">
                    <!--ÉO BIẾT DÂY LÀ CÁI GÌ LUÔN :|--> 
                     <p><label> Chi phí tham gia：</label><?php echo !empty($shop_info['shop_money_start']) ? $shop_info['shop_money_start'] : 'Liên hệ';?> </p>
                    <p><label> Số điện thoại：</label><?php echo $shop_info['phone'];?> </p>
                    <?php if(!empty(  $shop_info['shop_cmt'] )): ?><p><label> CMTND：</label><?php echo $shop_info['shop_cmt'];?> </p> <?php endif;?>
                    <?php if( !empty( $shop_info['address']['street_1'] ) ):?><p><label> Địa chỉ công ty：</label><?php echo $shop_info['address']['street_1']; ?> </p> <?php endif;?>
                   <p><label>   Liên kết：</label> <a href="<?php echo empty($shop_info['shop_url']) ? "#" :  trim($shop_info['shop_url']) ; ?>">
                           Trang web chính thức</a>
                    </p>
       

                </div>
            </div>
            <div class="col large-3 right-bottom-profile">
                <button class="yeu-cau-thong-tin">Nhận tư vấn</button>
            </div>
            <div class="menu-bottom-profile" id="nav_left_layout_user">
                <ul>
                <?php for( $i = 0 ; $i < sizeof($titles)  ; $i++ ):?>
                    <li><a href="#section<?php echo $i+1?>"><?php echo $titles[$i]?></a></li>
                <?php endfor?>
                </ul>
            </div>
        </div>
        <div class="row content">
            <div class="col large-9">
                
                
                <div class="artical" id="section1">
                    <div class="heading-title">
                        <h2><?php echo $titles[0] ?></h2>
                    </div>
                    <div class="content-article">
                       <?php echo !empty( $shop_info['editor_shop_general_info'] ) ? $shop_info['editor_shop_general_info'] : '' ;?>
                    </div>
                </div>


                
                <?php 

        
                    for( $i = 1 ; $i < 4 ; $i++ ):
                        
                ?>
                  
                <div class="artical" id="section<?php echo ($i+1)?>">
                    <div class="heading-title" >
                        <h2><?php echo $titles[$i];?></h2>
                    </div>
                    <div class="content-article">
                       <?php echo $shop_info[ $contens[$i-1] ];?>
                    </div>
                </div>
                
                <?php endfor; ?>
                <!--  hoi dap -->
                <div class="artical " id="section5">
                    <div class="heading-title" id="section<?php echo ($i+1)?>">
                        <h2>Hỏi đáp trực tuyến</h2>
                    </div>
                    <div class="content-article">
                        <div id="form-qa" class='form-qa' style="display:none"><?php echo do_shortcode('[contact-form-7 id="379" title="Chi tiết trang"]'); ?></div>
                        
                        <div class='form-qa'><?php echo do_shortcode('[contact-form-7 id="379" title="Chi tiết trang"]'); ?></div>
                    </div>
                </div>
                
                
            </div>
            
            <!--LEFT PROFILE--> 
            <div class="col large-3">
                
                <div class="artical shop-googlemap">
                    <div class="heading-title">
                        <h2>Bản đồ</h2>
                    </div>
                    <div class="content-article"> 

                      <iframe src="http://maps.google.com/maps?q=<?php echo $shop_info['location'] ?>&z=9&output=embed" width="100%" height="499" frameborder="0" style="border:0"></iframe>
                    </div>
                </div>
                
                 <div class="artical shop-facebook">
                    <div class="heading-title">
                        <h2>Facebook</h2>
                    </div>
                    <div class="content-article">
                        <div class="fb-page" data-href="<?php echo strip_tags($shop_info['shop_fb']) ?>" data-tabs="timeline" data-small-header="false" 
                             data-adapt-container-width="true" data-hide-cover="false" 
                             data-show-facepile="true">
                            <blockquote cite="<?php echo strip_tags($shop_info['shop_fb']) ?>" class="fb-xfbml-parse-ignore">
                                <a href="<?php echo $shop_info['shop_fb'];?>">Liên kết Facebook</a>
                            </blockquote>
                        </div>
                    </div>
                     
                </div><!-- .shop-facebook -->
                
                
                
                <div class="artical shop-ads">
                    <div class="heading-title">
                        <h2>Quảng cáo</h2>
                    </div>
                    <div class="content-article">
                        <?php 
                            $ads_right = cuongdc_get_qc_thuonghieu();
                            echo !empty( $ads_right ) ? $ads_right : ''; 
                            
                        ?>
                    </div>
                </div><!-- shop-ads -->

            </div><!--END LEFT PROFILE--> 
        </div>
    </div>


</div>
<script type="text/javascript">
  (function($){
      $(document).ready(function(){
           var noti = new Notyf();
        jQuery(".nav-brand-name").css("display","none");
        
        jQuery(".yeu-cau-thong-tin").on("click" , function(){
            jQuery("#form-qa").modal({
                  fadeDuration: 100
            }); 
        });
        
        $("#add-bookmark").on("click" , function(){
            $.ajax({
                url : "<?php echo admin_url('admin-ajax.php');?>",
                type: 'POST',
                data:{
                    action: "cuongdc_add_bookmark",
                    uid: parseInt($("#add-bookmark").attr("data-uid")),
                    nonce : "<?php echo wp_create_nonce() ?>"
                },
                success: function(data){
                        if( data == 'ok' ){
                            $("#add-bookmark").find(".fa").removeClass("fa-star-o").addClass("fa-star");
                           noti.confirm('Đã đánh dấu là yêu thích');
                        }else if(data == 'removed'  ){
                          $("#add-bookmark").find(".fa").removeClass("fa-star").addClass("fa-star-o");
                            noti.confirm('Đã bỏ đánh dấu');
                        }else if(data == 'login'){
                            noti.confirm('Bạn phải đăng nhập trước ');
                        }else{
                             noti.alert('Có lỗi xảy ra, xin thử lại sau :( ');
//                             window.location.href = window.location.href;
                        }
                 }
            });
        }); //bo thich 
      });//dom ready! 
  })(jQuery);

    jQuery.fn.smartFloat = function() {
        var position = function(element) {
            var top = element.position().top, pos = element.css("position");
            jQuery(window).scroll(function() {
                var scrolls = jQuery(this).scrollTop();
                
                if (scrolls > top) {
                    if (window.XMLHttpRequest) {
                        element.css({
                            position: "fixed",
                            width: "100%",
                            top: 30,
                            margin: "auto",
                            padding:0,
                            display:"block"
                        

                        });
                    } else {
                        element.css({
                            top: scrolls
                        });
                    }
                }else {
                    element.css({
                        position: pos,
                        top: top,
                        display:"none"
                    });
                }
            });
        };
        return jQuery(this).each(function() {
            position(jQuery(this));
        });
    };
    jQuery.fn.smartFloatmenu = function() {
        var position = function(element) {
            var top = element.position().top, pos = element.css("position");
            jQuery(window).scroll(function() {
                var scrolls = jQuery(this).scrollTop();
                if (scrolls > top) {
                    if (window.XMLHttpRequest) {
                        element.css({
                            position: "fixed",
                            width: "100%",
                            top: 86,
                            margin: "auto",
                            padding:0,
                            display:"block"


                        });
                    } else {
                        element.css({
                            top: scrolls
                        });
                    }
                }else {
                    element.css({
                        position: pos,
                        top: top
                    });
                }
            });
        };
        return jQuery(this).each(function() {
            position(jQuery(this));
        });
    };
    jQuery(".nav-brand-name").smartFloat();
    jQuery("#nav_left_layout_user").smartFloatmenu();


</script>

<style>

.nav-brand-name .button-brand-name button{
    letter-spacing: normal;
    font-size: 13px;
    font-weight: bold;
}
.nav-brand-name{
    z-index: 2;
}

.left-bottom-profile .info p{
        overflow: hidden;
    display: block;
    margin-bottom: 10px;    
}

#content-child .left-bottom-profile p label{
    padding-bottom: 0px;
    margin-bottom: 0;
    font-weight: normal;
    font-size: 18px;
    float: left;
}


#nav_left_layout_user{

}

#content-child .right-bottom-profile button{
    margin-top: 30%;
}

#content-child .text-profile .shop-name{
    font-size: 30px;
    font-weight: normal;    
    margin-bottom: 0px;
}

#content-child .text-profile .shop-short-desciption {
    font-size: 17px;
    font-weight: 100;
}
#add-bookmark:hover{
    cursor:pointer;
}
@media screen and (min-width: 1150px) {
    #content-child .menu-bottom-profile li {
        border-right: 0px;
        padding: 14px 46px;
    }

    #nav_left_layout_user li:last-child {
        border-right: 1px solid #CCC;

    }
}
.modal{
    margin-top:70px !important;
    z-index:9999999 !important;
    
}

.shop-ads img{
    width:100%;
    height:auto;
}
.blocker{
    z-index:999999999;
}
.nav-right-menu-mxh .fa-star {
    font-size: 22px;
    color: #ffd012;
}
.nav-right-menu-mxh .fa-star:hover {
    font-size: 24px;
    color: #ffd012;
}



.nav-right-menu-mxh #noi-bat {
    line-height: 48px;
}
</style>
<?php do_action( 'flatsome_after_page' ); ?>

<?php get_footer(); ?>
