<?php
/**
 * Dokan Settings Address form Template
 *
 * @since 2.4
 *
 * @package dokan
 */
?>

<?php

$address         = isset( $profile_info['address'] ) ? $profile_info['address'] : '';
$address_street1 = isset( $profile_info['address']['street_1'] ) ? $profile_info['address']['street_1'] : '';
$address_street2 = isset( $profile_info['address']['street_2'] ) ? $profile_info['address']['street_2'] : '';
$address_city    = isset( $profile_info['address']['city'] ) ? $profile_info['address']['city'] : '';
$address_zip     = isset( $profile_info['address']['zip'] ) ? $profile_info['address']['zip'] : '';
$address_country = isset( $profile_info['address']['country'] ) ? $profile_info['address']['country'] : '';
$address_state   = isset( $profile_info['address']['state'] ) ? $profile_info['address']['state'] : '';

?>

<input type="hidden" id="dokan_selected_country" value="<?php echo $address_country?>" />
<input type="hidden" id="dokan_selected_state" value="<?php echo $address_state?>" />
<div class="dokan-form-group">
    <label class="dokan-w3 dokan-control-label" for="setting_address"><?php _e( 'Địa chỉ ', 'dokan-lite' ); ?></label>

    <div class="dokan-w5 dokan-text-left dokan-address-fields">
        <?php if ( $seller_address_fields['street_1'] ) { ?>
            <div class="dokan-form-group">
                <label class="dokan-w3 control-label" for="dokan_address[street_1]"><?php _e( '', 'dokan-lite' ); ?>
                    <?php
                    $required_attr = '';
                    if ( $seller_address_fields['street_1']['required'] ) {
                        $required_attr = 'required'; ?>
                        <span class="required"> *</span>
                    <?php } ?>
                </label>
                <input <?php echo $required_attr; ?> <?php echo $disabled ?> id="dokan_address[street_1]" 
                    value="<?php echo esc_attr( $address_street1 ); ?>" 
                    name="dokan_address[street_1]" 
                    placeholder="<?php _e( 'Nhập địa chỉ thương hiệu' , 'dokan-lite' ) ?>" class="dokan-form-control input-md" type="text">
            </div>
        <?php } ?>

    </div>
</div>
