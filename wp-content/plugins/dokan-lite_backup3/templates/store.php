<?php
/**
 * Store page cho theme nay
 */

get_header();
do_action( 'flatsome_before_page' );


$user_id   = ( get_query_var( 'author' , false) );
$shop_info   = get_user_meta( $user_id, 'dokan_profile_settings' , true  );
$shop_name   = strip_tags(get_user_meta( $user_id, 'dokan_store_name' , true ));
 ?>

<div id="content-child" role="main" class="content-area">
    <div class="nav-brand-name" style="background-color: #974F15; ">
        <div class="container">
           <div class="row">
            <div class="col large-10">
                <div class="logo-brand-name">
                    <?php  echo wp_get_attachment_image( $shop_info['gravatar'] , 'normal' ) ?>
                </div>
                <div class="brand-name">
                    <ul>
                        <li>白面東 極品楊桃汁</li>
                        <li>加盟金： 請留言洽詢</li>
                    </ul>
                </div>
            </div>
               <div class="col large-2 button-brand-name">
                   <a href="#"><button>最新活動</button></a>
               </div>
           </div>
        </div>
    </div>
    <div class="container">
        <div class="row brearcrumb">
            <?php get_flatsome_breadcrumbs(); ?>
        </div>
        <div class="row img-cover">
            <div class="col large-12">
<!--                <img src="https://multisite.webchuanseo.design/wp-content/uploads/2017/12/20171123_7af5c.jpg">-->
                <?php  echo wp_get_attachment_image($shop_info['banner'] , 'full'  ) ?>
            </div>
        </div>
        <div class="row middle">
            <div class="col large-3">
                <div class=" img-profile">
<!--                    <img src="https://multisite.webchuanseo.design/wp-content/uploads/2017/12/20171123_025b0.jpg" />-->
                    <img src="<?php echo wp_get_attachment_image_url( $shop_info['gravatar'], 'normal' )?>" title="<?php echo $shop_name?>" />
                </div>
            </div>
            <div class="col large-9 text-profile">
                <h2 class="intro_in_0001 shop-name" ><?php echo $shop_name ?></h2>
                <p class="shop-short-desciption"><?php echo strip_tags( $shop_info['shop_short_intro'] ); ?></p>
            </div>
        </div>

        <div class="row bottom-profile" >
            <div class="col large-9 left-bottom-profile">
                <div class="info">
                    <!--ĐÉO BIẾT DÂY LÀ CÁI GÌ LUÔN :|--> 
                    <p><label>  加 盟 金：</label>請線上詢問或電話洽詢 </p>
                    <p><label>  加 盟 金：</label>請線上詢問或電話洽詢 </p>
                    <p><label>  加 盟 金：</label>請線上詢問或電話洽詢 </p>
                    <p><label>  加 盟 金：</label>請線上詢問或電話洽詢 </p>
                    <p><label>  加 盟 金：</label>請線上詢問或電話洽詢 </p>
                </div>
            </div>
            <div class="col large-3 right-bottom-profile">
                <button>電話洽詢</button>
            </div>
            <div class="menu-bottom-profile" id="nav_left_layout_user">
                <ul>
                    <li><a href="#">最新活動</a></li>
                    <li><a href="#">最新活動</a></li>
                    <li><a href="#">最新活動</a></li>
                    <li><a href="#">最新活動</a></li>
                    <li><a href="#">最新活動</a></li>

                </ul>
            </div>
        </div>
        <div class="row content">
            <div class="col large-9">
                <div class="artical">
                    <div class="heading-title">
                        <h2>影音介紹</h2>
                    </div>
                    <div class="content-article">
                        <?php echo $shop_info['editor_shop_intro'];?>
                    </div>
                </div>
                <div class="artical">
                    <div class="heading-title">
                        <h2>影音介紹</h2>
                    </div>
                    <div class="content-article">
                       <?php echo $shop_info['editor_shop_guide'];?>
                    </div>
                </div>
            </div>
            
            <!--LEFT PROFILE--> 
            <div class="col large-3">
                <div class="artical shop-googlemap">
                    <div class="heading-title">
                        <h2>Bản đồ</h2>
                    </div>
                    <div class="content-article"> 

                      <iframe src="http://maps.google.com/maps?q=<?php echo $shop_info['location'] ?>&z=9&output=embed" width="100%" height="499" frameborder="0" style="border:0"></iframe>
                    </div>
                </div>
                
                 <div class="artical shop-facebook">
                    <div class="heading-title">
                        <h2>Facebook</h2>
                    </div>
                    <div class="content-article">
                        <div class="fb-page" data-href="<?php echo strip_tags($shop_info['shop_fb']) ?>" data-tabs="timeline" data-small-header="false" 
                             data-adapt-container-width="true" data-hide-cover="false" 
                             data-show-facepile="true">
                            <blockquote cite="<?php echo strip_tags($shop_info['shop_fb']) ?>" class="fb-xfbml-parse-ignore">
                                <a href="<?php echo $shop_info['shop_fb'];?>">Liên kết Facebook</a>
                            </blockquote>
                        </div>
                    </div>
                     
                </div>

            </div>
            
            <!--END LEFT PROFILE--> 
        </div>
    </div>


</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".nav-brand-name").css("display","none");

    })
    jQuery.fn.smartFloat = function() {
        var position = function(element) {
            var top = element.position().top, pos = element.css("position");
            jQuery(window).scroll(function() {
                var scrolls = jQuery(this).scrollTop();
                
                if (scrolls > top) {
                    if (window.XMLHttpRequest) {
                        element.css({
                            position: "fixed",
                            width: "100%",
                            top: 0,
                            margin: "auto",
                            padding:0,
                            display:"block"
                        

                        });
                    } else {
                        element.css({
                            top: scrolls
                        });
                    }
                }else {
                    element.css({
                        position: pos,
                        top: top,
                        display:"none"
                    });
                }
            });
        };
        return jQuery(this).each(function() {
            position(jQuery(this));
        });
    };
    jQuery.fn.smartFloatmenu = function() {
        var position = function(element) {
            var top = element.position().top, pos = element.css("position");
            jQuery(window).scroll(function() {
                var scrolls = jQuery(this).scrollTop();
                if (scrolls > top) {
                    if (window.XMLHttpRequest) {
                        element.css({
                            position: "fixed",
                            width: "100%",
                            top: 56,
                            margin: "auto",
                            padding:0,
                            display:"block"


                        });
                    } else {
                        element.css({
                            top: scrolls
                        });
                    }
                }else {
                    element.css({
                        position: pos,
                        top: top
                    });
                }
            });
        };
        return jQuery(this).each(function() {
            position(jQuery(this));
        });
    };
    jQuery(".nav-brand-name").smartFloat();
    jQuery("#nav_left_layout_user").smartFloatmenu();


</script>

<style>
.nav-brand-name{
    z-index: 9999999999999999999;
}

#nav_left_layout_user{

}
</style>
<?php do_action( 'flatsome_after_page' ); ?>

<?php get_footer(); ?>
