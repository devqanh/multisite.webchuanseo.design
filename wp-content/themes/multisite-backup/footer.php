<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

 global $jQueryflatsome_opt;
?>

</main><!-- #main -->

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<?php  wp_footer(); ?>
<script type="text/javascript">
    jQuery.fn.smartFloat = function() {
        var position = function(element) {
            var top = element.position().top, pos = element.css("position");
            jQuery(window).scroll(function() {
                var scrolls = jQuery(this).scrollTop();
                if (scrolls > top) {
                    if (window.XMLHttpRequest) {
                        element.css({
                            position: "fixed",
                            width: "100%",
                            top: 30,
                            margin: "auto",
                            padding:0
                        });
                    } else {
                        element.css({
                            top: scrolls
                        });
                    }
                }else {
                    element.css({
                        position: pos,
                        top: top
                    });
                }
            });
        };
        return jQuery(this).each(function() {
            position(jQuery(this));
        });
    };

    jQuery(".nav_left_layout_user").smartFloat();

    jQuery(document).ready(function () {
        jQuery(document).on("scroll", onScroll);

        //smoothscroll
        jQuery('a[href^="#"]').on('click', function (e) {
            e.preventDefault();
            jQuery(document).off("scroll");

            jQuery('a').each(function () {
                jQuery(this).removeClass('active');
            })
            jQuery(this).addClass('active');

            var target = this.hash,
                menu = target;
            jQuerytarget = jQuery(target);
            jQuery('html, body').stop().animate({
                'scrollTop': jQuerytarget.offset().top+5
            }, 500, 'swing', function () {
                window.location.hash = target;
                jQuery(document).on("scroll", onScroll);
            });
        });
    });

    function onScroll(event){
        var scrollPos = jQuery(document).scrollTop();
        jQuery('#nav_left_layout_user a').each(function () {
            var currLink = jQuery(this);
            var refElement = jQuery(currLink.attr("href"));

                currLink.removeClass("active");

        });
    }
</script>
</body>
</html>
