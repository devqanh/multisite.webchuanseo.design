<?php

function czc_disable_extra_image_sizes() {
	foreach ( get_intermediate_image_sizes() as $size ) {
		remove_image_size( $size );
	}
}
// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );

register_nav_menus(
        array(
                'topbar-left' => 'Menu left top bar',
                'topbar-right' => 'Menu right top bar', 
                'bottom-header' => 'Menu bottom header' ,
                'fixed-bottom-home' => "Menu fixed bottom home",
	            'custom-page-ct' => "Menu page chi tiet",
	            'category-list-brand' => "Menu category list thương hiệu"
        )
);
//create sidebar custom button page chi tiet
register_sidebar(array(
	'name' => 'Button trang chi tiết',
	'id' => 'button-page-menu-chi-tiet',
	'description' => 'Khu vực sidebar hiển thị menu trang chi tiết',
));
/*Creating shortcode for menu*/
function list_menu($atts, $content = null) {
    extract(shortcode_atts(array(
        'menu'            => '',
        'container'       => '',
        'container_class' => '',
        'container_id'    => '',
        'menu_class'      => '',
        'menu_id'         => '',
        'echo'            => true,
        'fallback_cb'     => 'wp_page_menu',
        'before'          => '',
        'after'           => '',
        'link_before'     => '',
        'link_after'      => '',
        'depth'           => 0,
        'walker'          => '',
        'theme_location'  => ''),
        $atts));

    return wp_nav_menu( array(
        'menu'            => $menu,
        'container'       => $container,
        'container_class' => $container_class,
        'container_id'    => $container_id,
        'menu_class'      => $menu_class,
        'menu_id'         => $menu_id,
        'echo'            => false,
        'fallback_cb'     => $fallback_cb,
        'before'          => $before,
        'after'           => $after,
        'link_before'     => $link_before,
        'link_after'      => $link_after,
        'depth'           => $depth,
        'walker'          => $walker,
        'theme_location'  => $theme_location));
}
add_shortcode("menu_fixed_home", "list_menu");
/*End*/


add_action( 'wp_enqueue_scripts' , function(){
    if( is_page('brands') || is_search() ){
        wp_enqueue_style( 'cuongdccss_listthuonghieu' , get_stylesheet_directory_uri() . '/css/page_listthuonghieu.css' );
    }
    
    if( dokan_is_store_page() || is_page('brands') || is_page('hoi-dap')  ){
        wp_enqueue_script('modaljs' , 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js' , 'jquery' , '1.0' , true );
        wp_enqueue_style('modalcss' , 'https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css');
        
    }
    
    if( is_page('kho-clip') ){
        wp_enqueue_style('fancybox-css' ,get_stylesheet_directory_uri() . '/css/jquery.fancybox.css');
        wp_enqueue_script('fancybox-js' , get_stylesheet_directory_uri() . '/js/jquery.fancybox.pack.js' , 'jquery' , '1.0' , true);
    }

} );

// doi title theo trang thuong hieu 
add_filter( 'document_title_parts' , function($title_parts){
    if(  is_page('brands') ){
        $title_parts['title'] = ucfirst(cuongdc_get_ten_thuonghieu_list_by_slug( $_GET['t'] ));
    }
    else{
        $store_name = get_query_var( 'b' );
        if(  !empty( $store_name ) ){
            $title_parts['title'] = ($store_name);   
        }
    }
    return $title_parts;
}, 10 , 1  );




// add_filter('pre_get_posts', function($query){
    
// if ($query->is_search && !is_admin() ) {
//   wp_redirect();
// }
    
// } );
//register sidebar quang cao
register_sidebar( array(
	'name' =>__( 'Quảng cáo header', 'wpb'),
	'id' => 'quangcao-header',
	'description' => __( 'Quảng cáo trên header', 'wpb' ),
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
) );
