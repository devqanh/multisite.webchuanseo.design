
<?php
/*
 Template Name: hoi dap
*/
get_header(); ?>

<?php do_action( 'flatsome_before_page' ); ?>

<div id="content-hoidap" role="main" class="content-area">
        <div class="row middle">
                <div class="col large-9">
                        <div class="hoi-dap">
            				<?php echo do_shortcode('[contact-form-7 id="396" title="Trang hỏi đáp"]'); ?>

			           </div><!--ho dap-->
			<div class="main-list">
                <div class="title-span">
					<?php 
					    $the_query = new WP_Query(array(
					    'post_type' => 'wpcf7s' , //cotact form 7 submission post type
					    'posts_per_page' => 12, 
					    'paged' =>  get_query_var('paged'),
					    
					   ));
					   
                    ?>
                    <!--hoi dap count -->
                    Đã có <?php echo $the_query->post_count ?>  người đặt câu hỏi

                </div>

					<?php 
					    while( $the_query->have_posts() ):
					        $the_query->the_post();
					        
					        $postId = get_the_Id();
					        
	                if( empty( get_post_meta( $postId , 'wpcf7s_posted-ho-va-ten' , true  )  )  
                      || empty( get_post_meta( $postId , 'wpcf7s_posted-loai-hinh-thuong-hieu' , true ) )  ) continue;
					        
                            $nguoiHoi =         cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-ho-va-ten' , true  ) );
                            $sdt =              cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-tel-137' , true ) ) ;
                            $loaiThuongHieu =   cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-loai-hinh-thuong-hieu' , true )[0] );
                            $email  =           cuongdc_trim_strip( get_post_meta($postId, 'wpcf7s_posted-email-215' , true) );
                            $diaChiMoShop =      cuongdc_trim_strip( get_post_meta($postId, 'wpcf7s_posted-dia-chi-mo-shop' , true)[0] );
                            $matTien     =   cuongdc_trim_strip(get_post_meta($postId , 'wpcf7s_posted-mat-tien' , true )[0] ); // mật tiền
                            $dauTuFrom =      cuongdc_trim_strip(get_post_meta( $postId , 'wpcf7s_posted-du-kien-dau-tu-from' , true ) );
                            $dauTuTo   =    cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-du-kien-dau-tu-to'  , true) );
                            $thoiGianMo =     cuongdc_trim_strip(get_post_meta( $postId , 'wpcf7s_posted-du-kien-khai-truong' , true ) );
                            $thoiGianGap =   cuongdc_trim_strip(get_post_meta($postId , 'wpcf7s_posted-co-the-lien-he' , true ) );
                            $chuThich   =     cuongdc_trim_strip(get_post_meta( $postId , 'wpcf7s_posted-chu-thich'  , true ) );
					      
					        $hidden_email_arr = explode( "@" ,$email );
					        $hidden_email = substr_replace( $hidden_email_arr[0] , "******"  , 2) . "@".$hidden_email_arr[1];
					        
					        $hidden_sdt = substr_replace( $sdt , "****" , 4 , (sizeof($sdt)-1) - 2 );

					       // var_dump($postId);
					    
					?>
					<div class="main-list-content">
						<div class="main-list-new row">
							<div class="main-list-content-left col large-8">
									<a href="#"><h2 class="h2"><?php echo $nguoiHoi ?>：<?php echo $loaiThuongHieu ?></h2></a>
                                <div class="row">
									<div class="main-list-left col large-6">
											<p>Địa điểm：<?php echo $matTien ?></p>
											<p>Thời gian mua ：<?php echo $thoiGianMo ?></p>

									</div>
									<div class="main-list-right col large-6">
											<p>Đã có cửa hàng：<?php echo $diaChiMoShop?></p>
											<p>Thời gian liên lạc：<?php echo $thoiGianGap ?></p>
									</div>
                                </div>
									<div class="main-nd">Ghi chú： <?php echo $chuThich?></div>
							</div><!--main-list-content-left-->
							<div class="main-list-content-right col large-4">
									<div class="info">
											Ngân sách dự kiến
											<h2 class="h2"><?php echo $dauTuFrom ?>~<?php echo $dauTuTo?></h2>
									</div><!--infon-->
									<div class="info-btn">
									        <div class="hidden-sdt" style="display:none"><?php echo $hidden_sdt ?></div>
									        <div class="hidden-email" style="display:none"><?php echo $hidden_email ?></div>
											<button type="submit" class="btn1 btn-tuvan"> Tư vấn</button>
									</div>
							</div><!--main-list-content-right-->
						</div><!--main-list-new-->
					</div><!--main-list-cnontent-->
					
                    <?php 
                        endwhile;
                        wp_pagenavi( array(
                            'query' => $the_query    
                        ) );
                        wp_reset_postdata();
                    ?>
				

            

			</div><!--main-list-->
                    
                    
                
                </div>
                
                <div class="col large-3">
                    <div class="man-right">
                        <div class="row section-title-custom-sidebar">
                    <h3 class="section-title section-title-normal">
                        <b></b>
                        <span class="section-title-main">熱門推薦</span>
                        <b></b>
                    </h3>
                </div>
					<div class="main-right-bar">

						<div class="main-right-content">
							<div class="main-right-content-img">
								<img src="http://yesone.com.tw/ally/images/banner/l_index/2017_2.jpg">
							</div><!--mian-right-content-img-->
							<div class="main-right-content-title">
									<a href="#"><h2>線上最大連鎖加盟展</h2></a>
							</div><!--main-right-content-title-->
						</div><!--main-right-content-->

					</div><!--main-right-bar-->

				<div class="row section-title-custom-sidebar">
                    <h3 class="section-title section-title-normal">
                        <b></b>
                        <span class="section-title-main">熱門推薦</span>
                        <b></b>
                    </h3>
                </div>
					<div class="main-right-list">

						<div class="main-right-list-content">
							<div class="main-right-list-img">
								<img src="http://yesone.com.tw/ally/album/O2017112486640_logo/a_img/20171127_d7f6a.jpg">
							</div><!--mian-right-content-img-->
							<div class="main-right-list-title">
									<a href="#"><p>線上最大連鎖加盟展</p></a>
									
							</div><!--main-right-content-title-->
						</div><!--main-right-content-->

						<div class="main-right-list-content">
							<div class="main-right-list-img">
								<img src="http://yesone.com.tw/ally/album/O2017112486640_logo/a_img/20171127_d7f6a.jpg">
							</div><!--mian-right-content-img-->
							<div class="main-right-list-title">
									<a href="#"><p>線上最大連鎖加盟展</p></a>
							</div><!--main-right-content-title-->
						</div><!--main-right-content-->

						<div class="main-right-list-content">
							<div class="main-right-list-img">
								<img src="http://yesone.com.tw/ally/album/O2017112486640_logo/a_img/20171127_d7f6a.jpg">
							</div><!--mian-right-content-img-->
							<div class="main-right-list-title">
									<a href="#"><p>線上最大連鎖加盟展</p></a>
							</div><!--main-right-content-title-->
						</div><!--main-right-content-->

					</div><!--main-right-list-->
					<div style="clear:left;"></div>
					
					<div class="main-right-qc">
						<img src="http://yesone.com.tw/ally/images/banner/list_r/20170504_2.jpg">
					</div><!--main-right-pc-->
                </div>
                </div>
            </div>
                
        </div>
</div>

<div id="form-hoi-dap-tu-van-modal">
   <div class="hoi-dap-tu-van">
       <div class="title">
           <h4>Tôi muốn liên lạc với anh ấy</h4>
       </div>
       <div class="content">
           <p class="sdt"><label>Số liên lạc:</label><span id="hidden-sdt">0989-XXX-256</span></p>
           <p class="email"><label>E-mail:</label><span id="hidden-email">john8 @ ***********</span></p>
           <p class="content-chitiet">
           <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Những lý do sau khiến </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">bạn </font><font style="vertical-align: inherit;">không thể duyệt qua thông tin đầy đủ: </font><font style="vertical-align: inherit;">
            
                <br/>Trước tiên, bạn chưa công bố thông tin về thương hiệu chi nhánh [ </font></font><a href="http://yesone.com.tw/ally/index_new.php" target="_blank">
                               
                <br/><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">được xuất bản</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ]. </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                       Thứ hai, bạn đã xuất bản thương hiệu nhượng quyền, nhưng chưa đăng ký thành viên [ </font></font><a href="http://yesone.com.tw/join/login.php?url=http://yesone.com.tw/ally/buy.php" target="_blank"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">thành viên đăng nhập</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ].</font></font>
               <font style="vertical-align: inherit;">
                   
                   <br/><font style="vertical-align: inherit;">Những lý do sau khiến </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                       bạn </font><font style="vertical-align: inherit;">không thể duyệt qua thông tin đầy đủ: </font><font style="vertical-align: inherit;">Trước tiên, bạn chưa công bố thông tin về thương hiệu chi nhánh [ </font></font><a href="http://yesone.com.tw/ally/index_new.php" target="_blank"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">được xuất bản</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ]. </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
           </p>
       </div>
   </div>
    
</div><!-- Form modal goese here ! -->

<script>
    (function($){
        $(document).ready(function(){
            console.log('hoidap ready!');
            $(".btn-tuvan").on("click" , function(){
                var sdt   =  $(this).parent().find(".hidden-sdt").text();
                var email =  $(this).parent().find(".hidden-email").text();
                
                //console.log("sdt : " + sdt + "| email:" + email );
               
                $("#hidden-sdt").text( sdt );
                $("#hidden-email").text( email );
                
                $("#form-hoi-dap-tu-van-modal").modal({
                     fadeDuration: 100
                }) ;
            });
            
        });   
        
    })(jQuery); 
</script>

</script>
<?php do_action( 'flatsome_after_page' ); ?>

<?php get_footer(); ?>


