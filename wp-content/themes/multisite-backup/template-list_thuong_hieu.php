<?php
/**
 * Template Name: Page List Thuong Hieu
 */
 if( empty( $_GET['t'] ) ){
    global $wp_query;
    $wp_query->set_404();
    status_header( 404 );
    get_template_part( 404 ); exit();
   exit;
}
get_header(); 

$loai_thuong_hieu = cuongdc_get_thuonghieu_list_by_name( cuongdc_trim_strip( $_GET['t'] ) );

?>

<?php do_action( 'flatsome_before_page' ); ?>

<div id="content-list-child" role="main" class="content-area">
	<div class="container">
		<div class="row brearcrumb">
			<?php get_flatsome_breadcrumbs(); ?>
		</div>
        <div class="gap-element" style="display:block; height:auto; padding-top:10px"></div>
        <div class="row section-title-custom">
            <h3 class="section-title section-title-normal">
                <b></b>
                <span class="section-title-main">Thương hiệu nổi bật</span>
                <b></b>
                <!--<a href="#" target="">刊登加盟</a>-->
            </h3>
        </div>

        <div class="gap-element" style="display:block; height:auto; padding-top:30px"></div>

		<div class="row banner-noibat">
		    <?php 
   if( !empty( $loai_thuong_hieu['top_3_noi_bat'] ) && is_array( $loai_thuong_hieu['top_3_noi_bat'] ) ):
        $top3 = $loai_thuong_hieu['top_3_noi_bat'];
		    ?>
			<div class="col large-4">
			    <?php if( !empty( $top3[0] ) && is_array( $top3[0] ) ): ?>
        		 	<div class="banner-1 zoom-img">
    					<a href=" <?php echo $top3[0]['link'] ?>" title="<?php echo $top3[0]['tieu_de']?>">
    					    <?php echo wp_get_attachment_image( $top3[0]['anh_noi_bat'] , 'large' );?>    
    					</a>
    				</div>
			    <?php else: ?>
				<div class="banner-1 zoom-img">
					<a href="#"><img src="http://yesone.com.tw/ally/album/O2013062865948_logo/a_img/20170630_65895.jpg"/></a>
				</div>
				<?php endif;?>
				
				 <?php if( !empty( $top3[1]) && is_array( $top3[1] ) ):?>
        		 	<div class="banner-1 zoom-img">
    					<a href=" <?php echo $top3[1]['link'] ?>" title="<?php echo $top3[1]['tieu_de']?>">
    					    <?php echo wp_get_attachment_image( $top3[1]['anh_noi_bat'] , 'large' );?>    
    					</a>
    				</div>		 
				 <?php else: ?>
    				<div class="banner-2 zoom-img">
    					<a href="#"><img src="http://yesone.com.tw/ally/album/O2017062848149_logo/a_img/20170922_e089b.jpg"/></a>
    				</div>
				<?php endif;?>
			</div>
			
			 <?php if( !empty( $top3[2] ) && is_array( $top3[2] ) ): ?>
     	 		<div class="col large-8 zoom-img">
    				<a href=" <?php echo $top3[2]['link'] ?>" title="<?php echo $top3[2]['tieu_de'] ?>">
    				  <?php echo wp_get_attachment_image( $top3[2]['anh_noi_bat'] , 'full' ); ?>    
    			    </a>
    			</div>
			 <?php else: ?>
    			<div class="col large-8 zoom-img">
    				<a href="#"><img src="http://yesone.com.tw/ally/album/O2017083111863_logo/a_img/20170919_e4d97.jpg"/> </a>
    			</div>
    			
			<?php endif; ?>
			
	        <?php else: ?>
	        	<div class="col large-4">
			    	<div class="banner-1 zoom-img">
				    	<a href="#"><img src="http://yesone.com.tw/ally/album/O2013062865948_logo/a_img/20170630_65895.jpg"/></a>
				    </div>
				    
    				<div class="banner-2 zoom-img">
    					<a href="#"><img src="http://yesone.com.tw/ally/album/O2017062848149_logo/a_img/20170922_e089b.jpg"/></a>
    				</div>
    				
    			 </div>
    			 
    			<div class="col large-8 zoom-img">
    				<a href="#"><img src="http://yesone.com.tw/ally/album/O2017083111863_logo/a_img/20170919_e4d97.jpg"/> </a>
    			</div>
	    
 <?php 	endif; //top 3 noi bat ?>
		</div>

        <div class="gap-element" style="display:block; height:auto; padding-top:30px"></div>

        <div class="row section-title-custom">
            <h3 class="section-title section-title-normal">
                <b></b>
                <span class="section-title-main">Danh sách thương hiệu</span>
                <b></b>
                <!--<a href="#" target="">刊登加盟</a>-->
            </h3>
        </div>

        <div class="gap-element" style="display:block; height:auto; padding-top:30px"></div>

        <div class="row logo-thuong-hieu">
            
            <?php 
                $thuong_hieu_noi_bat = $loai_thuong_hieu['thuong_hieu_noi_bat'];
                if( is_array( $thuong_hieu_noi_bat ) && !empty( $thuong_hieu_noi_bat ) ):
                    foreach( $thuong_hieu_noi_bat as $user_id  ):
                    $th = cuongdc_get_user_thuonghieu_by_id($user_id);
                    
                   // var_dump(  $th );
                    
                        if( !empty(  cuongdc_trim_strip($th['store_slug']) )  && !empty( $th['store_settings'] )):
                ?>
                        <div class="col-logo-thuong-hieu">
                                <div class="img-logo-thuong-hieu zoom-img">
                                    <a href="/b/<?php echo $th['store_slug']?>" title="<?php echo $th['store_settings']['store_name'] ?>" >
                                        <?php
                                            if( !empty( cuongdc_trim_strip($th['store_settings']['gravatar'])) )
                                                     echo wp_get_attachment_image( $th['store_settings']['gravatar'] , 'normal' ) ?>
                                    </a>
                                </div>
    
                            <div class="title-logo-thuong-hieu">
                                <h5><a href="/b/<?php $th['store_slug']?>"><?php echo $th['store_settings']['store_name']; ?></a></h5>
                            </div>
                        </div>
                            
                <?php 
                        endif;
                endforeach; 
            endif;
         ?>

        </div>

        <!-- <div class="gap-element" style="display:block; height:auto; padding-top:30px"></div> -->


        <!-- START LIST THUONG HIEU  -->

     <div id="main" class="list-thuong-hieu-wrapper" style="width:100%">
        <div class="row section-title-custom">
          <h3 class="section-title section-title-normal">
                <b></b>
                <span class="section-title-main">Tất cả thương hiệu</span>
                <b></b>
                <a href="#" target="">Tất cả</a>
            </h3>
        </div>


        <div id="main-left">
          <div class="main-left-content">

<?php 
            $th_ids = $loai_thuong_hieu['thuong_hieu_trong_danh_sach'];
            
            if( !empty( $th_ids ) && is_array($th_ids) ):

            foreach( $th_ids as $user_id ):
            
            $cuaHang = cuongdc_get_user_thuonghieu_by_id( $user_id );

?>
            <div class="main-content">
              <div class="main-content-new">
                <div class="main-content-left">
                    <a href="/b/<?php echo $cuaHang['store_slug'] ?>" title="<?php echo $cuaHang['store_settings']['store_name'] ?>">
                    <?php echo wp_get_attachment_image(  $cuaHang['store_settings']['gravatar'] , 'normal', array(
                            'class' => 'thuonghieu-image'
                        ) ); ?>
                    </a>
                </div>
                <!--main-content-left-->
                <div class="main-content-center">
                  <a href="/b/<?php echo $cuaHang['store_slug'] ?>"><h2> <?php echo $cuaHang['store_settings']['store_name'] ?>  </h2></a>

                  <p><?php echo wp_trim_words( trim( strip_tags( $cuaHang['store_settings']['shop_short_intro'] ) ) , 120 , ' ...' )?></p>
                </div>
                <!---mian-content-center-->
                <div class="main-content-right">
                  <div class="main-content-right-info">
                    <h2>Yêu cầu</h2>
                  </div>
                  <!--main-content-right-info-->
                  <div class="main-content-right-btn">
                    <a>
                        <div class="hidden-sdt" style="display:none"><?php echo $hidden_sdt ?></div>
                        <div class="hidden-email" style="display:none"><?php echo $hidden_email ?></div>
                        <span style="display:none" class="uid"><?php echo $user_id ?></span>
                        <button class="thuonghieu-btn-request" type="submit">Yêu cầu</button>
                            
                    </a>
                  </div>
                  <!--main-content-right-btn-->
                </div>
                <!--main-content-right-->
              </div>
              <!--main-content-new-->
            </div>
            <!--main-content-->
        <?php endforeach; 
              endif;
        ?>

            
          </div> <!--main-left-content-->


          <div class="main-left-list">
            <div class="row section-title-custom-sidebar">
              <h3 class="section-title section-title-normal">
                        <b></b>
                        <span class="section-title-main">Yêu cầu mới nhất</span>
                        <b></b>
                        <a href="/hoi-dap" target="_blank" style="font-size: 15px;margin-right: 20px;text-align:right;">Xem tất cả</a>
                    </h3>
            </div>

            <?php 
                    $the_query = new WP_Query(array(
            		    'post_type' => 'wpcf7s' , //cotact form 7 submission post type
            		    'posts_per_page' => 8, 
            		    'paged' =>  get_query_var('paged'),
            	   ));
            	   while( $the_query->have_posts() ):
            	       $the_query->the_post();
                        
                        $postId = get_the_Id();
                        
                      if( empty( get_post_meta( $postId , 'wpcf7s_posted-ho-va-ten' , true  )  )  
                      || empty( get_post_meta( $postId , 'wpcf7s_posted-loai-hinh-thuong-hieu' , true ) )  ) continue;
                          
                        $nguoiHoi =         cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-ho-va-ten' , true  ) );
                        $sdt =              cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-tel-137' , true ) ) ;
                        $loaiThuongHieu =   cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-loai-hinh-thuong-hieu' , true )[0] );
                        $email  =           cuongdc_trim_strip( get_post_meta($postId, 'wpcf7s_posted-email-215' , true) );
                        $diaChiMoShop =      cuongdc_trim_strip( get_post_meta($postId, 'wpcf7s_posted-dia-chi-mo-shop' , true));
                        $matTien     =   cuongdc_trim_strip(get_post_meta($postId , 'wpcf7s_posted-mat-tien' , true )[0] ); // mật tiền
                        $dauTuFrom =      cuongdc_trim_strip(get_post_meta( $postId , 'wpcf7s_posted-du-kien-dau-tu-from' , true ) );
                        $dauTuTo   =    cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-du-kien-dau-tu-to'  , true) );
                        $thoiGianMo =     cuongdc_trim_strip(get_post_meta( $postId , 'wpcf7s_posted-du-kien-khai-truong' , true ) );
                        $thoiGianGap =   cuongdc_trim_strip(get_post_meta($postId , 'wpcf7s_posted-co-the-lien-he' , true ) );
                        $chuThich   =     cuongdc_trim_strip(get_post_meta( $postId , 'wpcf7s_posted-chu-thich'  , true ) );
                        
                      ;
                        
                        $hidden_email_arr = explode( "@" ,$email );
                        $hidden_email = substr_replace( $hidden_email_arr[0] , "******"  , 2) . "@".$hidden_email_arr[1];
                        
                        $hidden_sdt = substr_replace( $sdt , "****" , 4 , (sizeof($sdt)-1) - 2 );
                    
					   
             ?>
                <div class="main-left-list-content modal-ask">
                    <span style="display:none" class="hidden-email"><?php echo $hidden_email ?></span>
                    <span style="display:none" class="hidden-sdt"><?php echo $hidden_sdt ?></span>
                  <a href="#">
                    <font color="#9A450E"><?php echo $nguoiHoi ?>：</font><?php echo $loaiThuongHieu ?>，Địa điểm：<?php echo $diaChiMoShop ?> ，Dự kiến khai trương：<?php echo $thoiGianMo ?> ，
                    <font color="red"><b>Kinh phí <?php echo $dauTuFrom ?>~<?php echo $dauTuTo ?> </b></font>
                  </a>
                </div>
                <!--main-left-list-content-->
            <?php endwhile; ?>        
            
            

          </div>
          <!--main-left-list-->
          <div class="main-left-check brand-hoi-dap-modal" id="brand-hoi-dap" style="display:none">
              <?php echo do_shortcode('[contact-form-7 id="433" title="Hỏi đáp trực tuyến - Trang cá danh sách thương hiệu"]') ; ?>
          </div>
          
          <div class="main-left-check brand-hoi-dap-form" id="brand-hoi-dap">
              <?php echo do_shortcode('[contact-form-7 id="433" title="Hỏi đáp trực tuyến - Trang cá danh sách thương hiệu"]') ; ?>
          </div>
          
          

          <!--main-left-check-->

        </div>
        <!--main-left-->


        <!-- START MAIN RIGHT  -->
        <div id="main-right">
            <?php 
             $ads_right = $loai_thuong_hieu['qcright_loop'];
         if( !empty( $ads_right ) ):
          
                foreach( $ads_right as $ad_right_wrapper ):
            ?>
               <div class="ad-right-wrapper">
                  <div class="row section-title-custom-sidebar">
                    <h3 class="section-title section-title-normal">
                        <b></b>
                        <span class="section-title-main"><?php echo trim(strip_tags( $ad_right_wrapper['qcright_title'] ));?></span>
                        <b></b>
                    </h3>
                  </div>
                  <div class="main-right-bar">

                    <?php foreach( $ad_right_wrapper['qcright_ads'] as $ad ): 
                            if( empty( $ad['qcright_ads_image'] ) ){
                    ?>
                            <div class="main-left-list-content">
                              <a href="<?php echo cuongdc_trim_strip($ad['qcright_ads_link']);?>">
                                <font color="#9A450E"><?php echo cuongdc_trim_strip($ad['qcright_ads_title']);?></font>
                              </a>
                            </div>
                    <?php 
                            }else{
                    ?>
                        <div class="main-right-content">
                          <div class="main-right-content-img">
                            <a href="<?php echo cuongdc_trim_strip($ad['qcright_ads_link']) ?>" 
                                title="<?php echo cuongdc_trim_strip( $ad['qcright_ads_title'] ) ?>">
                                 <?php echo wp_get_attachment_image( $ad['qcright_ads_image'] , 'normal'  ) ?>
                             </a>
                          </div>
                          <!--mian-right-content-img-->
                          <div class="main-right-content-title">
                            <a href="<?php echo cuongdc_trim_strip($ad['qcright_ads_link']) ?>">
                                <h2><?php echo  cuongdc_trim_strip( $ad['qcright_ads_title']  );?></h2></a>
                          </div>
                          <!--main-right-content-title-->
                        </div>
                        <!--main-right-content-->
                    <?php };

                    endforeach?>
                </div>
                  <!--main-right-bar-->
                
                 <div class="row section-title-custom-sidebar brand-custom-sidebar">
                    <h3 class="section-title section-title-normal">
                            <span class="section-title-main">DAnh mục thương hiệu</span
                  </h3>  
                   <div class="main-right-bar">
                    <?php wp_nav_menu( array(
                        'theme_location' => 'category-list-brand',
                        'menu_class' => 'nav-list-brand'
                    ) ); ?>
                    </div>
                </div><!-- .brand-custom-sidebar -->
                    
            </div><!-- .ad-right-wrapper -->
      <?php endforeach; endif; ?>
     

      <!--main-right-category-->

        </div><!-- main-right-wrapper -->
    </div>
        <!--main-right-->

      </div>
        <!-- END LIST THUONG HIEU  -->
	</div>
</div>


<!--FORM HOI DAP MODAL-->

<div id="form-hoi-dap-tu-van-modal">
   <div class="hoi-dap-tu-van">
       <div class="title">
           <h4>Tôi muốn liên lạc với anh ấy</h4>
       </div>
       <div class="content">
           <p class="sdt"><label>Số liên lạc:</label><span id="hidden-sdt"></span></p>
           <p class="email"><label>E-mail:</label><span id="hidden-email"></span></p>
           <p class="content-chitiet">
           <font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Những lý do sau khiến </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">bạn </font><font style="vertical-align: inherit;">không thể duyệt qua thông tin đầy đủ: </font><font style="vertical-align: inherit;">
            
                <br/>Trước tiên, bạn chưa công bố thông tin về thương hiệu chi nhánh [ </font></font><a href="http://yesone.com.tw/ally/index_new.php" target="_blank">
                               
                <br/><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">được xuất bản</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ]. </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                       Thứ hai, bạn đã xuất bản thương hiệu nhượng quyền, nhưng chưa đăng ký thành viên [ </font></font><a href="http://yesone.com.tw/join/login.php?url=http://yesone.com.tw/ally/buy.php" target="_blank"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">thành viên đăng nhập</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ].</font></font>
               <font style="vertical-align: inherit;">
                   
                   <br/><font style="vertical-align: inherit;">Những lý do sau khiến </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
                       bạn </font><font style="vertical-align: inherit;">không thể duyệt qua thông tin đầy đủ: </font><font style="vertical-align: inherit;">Trước tiên, bạn chưa công bố thông tin về thương hiệu chi nhánh [ </font></font><a href="http://yesone.com.tw/ally/index_new.php" target="_blank"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">được xuất bản</font></font></a><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> ]. </font></font><br><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">
           </p>
       </div>
   </div>
   
<!--FORM HOI DAP MODAL-->
</div><!-- Form modal goese here ! -->

<div style="display:none"  class="modal" id="brand-hoi-dap"><?php echo do_shortcode ('[contact-form-7 id="433" title="Hỏi đáp trực tuyến - Trang cá danh sách thương hiệu"]'); ?> </div>

<style type="text/css">

.main-left-list .main-left-list-content{
    height: auto !important;
}
.img-logo-thuong-hieu img{
    width: 220px;
height: 142px;
}
  .modal{
    max-width: 800px;
  }

  #form-qa .form-header input{
  margin-left: 0px;
}



  #form-qa .form-group{
  overflow: hidden;
  background: #FFFFE6;
}

#form-qa input{
  margin-left: 0px;
}

@media screen and ( min-width: 959px ){

#form-qa .form-group .form-label{
  width: 30%;
  float: left;
  display: block;

  
}

#form-qa .form-group .form-input{
  width: 70%;
  background: white;
  float: left;

}

#form-qa .form-header{
  color: #383835;
  text-align: center;
  width: 100%;
  background: #F3F3F3;
  padding: 6px;
}


#form-qa .du-kien-dau-tu-from, #form-qa .du-kien-dau-tu-to{
  float: left;

}

#form-qa .du-kien-dau-tu-to select, #form-qa .du-kien-dau-tu-from select{

  margin: 0px;
  padding-top : 5px;
  padding-bottom : 5px;
  font-size: 14px;
  padding-left: 10px;
  padding-right: 10px;
  
}


#form-qa .sep{

  width: 20px;
  float: left;
}

}/**959px **/

</style>
<script>
  (function($){
    $(document).ready(function(){
        console.log('brand ready! ');
        $(".thuonghieu-btn-request").on("click", function(){
            $(".brand-hoi-dap-modal input[name=uid]").val( parseInt($(this).parent().find(".uid").text()) );
            $(".brand-hoi-dap-modal").modal();
        });
        
        $(".modal-ask").on("click" , function(){
                var sdt = $(this).find(".hidden-sdt").text().trim();
                var email = $(this).find(".hidden-email").text().trim();
                
                $("#hidden-sdt").text( sdt );
                $("#hidden-email").text( email );

                $("#form-hoi-dap-tu-van-modal").modal();
        });
        
    });    
  })(jQuery);
  
  
</script> 


<?php while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>

<?php endwhile; // end of the loop. ?>

<?php do_action( 'flatsome_after_page' ); ?>

<?php get_footer(); 