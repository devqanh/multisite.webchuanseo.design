<div id="masthead" class="header-main <?php header_inner_class( 'main' ); ?>">
    <div class="header-inner flex-row container <?php flatsome_logo_position(); ?>" role="navigation">

        <!-- Logo -->
	    <?php if ( is_front_page()  || wp_is_mobile() ||  is_page(282) || is_page(338)): ?>
            <div id="logo" class="flex-col logo">
				<?php get_template_part( 'template-parts/header/partials/element', 'logo' ); ?>
            </div>

	    <?php endif; ?>
        <!-- Mobile Left Elements -->
        <div class="flex-col show-for-medium flex-left">
            <ul class="mobile-nav nav nav-left <?php flatsome_nav_classes( 'main-mobile' ); ?>">
				<?php flatsome_header_elements( 'header_mobile_elements_left', 'mobile' ); ?>
            </ul>
        </div>

        <!-- Left Elements -->
        <div class="flex-col hide-for-medium flex-left
            <?php if ( get_theme_mod( 'logo_position', 'left' ) == 'left' ) {
			echo 'flex-grow';
		} ?>">
            <ul class="header-nav header-nav-main nav nav-left <?php flatsome_nav_classes( 'main' ); ?>">
				<?php flatsome_header_elements( 'header_elements_left' ); ?>
            </ul>
        </div>
        <!-- Right Elements -->
		<?php if ( is_front_page() ||  is_page(282) ||is_page(338) ): ?>
            <div class="flex-col hide-for-medium flex-right">
                <ul class="header-nav header-nav-main nav nav-right <?php flatsome_nav_classes( 'main' ); ?>">
					<?php flatsome_header_elements( 'header_elements_right' ); ?>
                </ul>
	            <?php wp_nav_menu( array(
		            'theme_location' => 'bottom-header',
		            'menu_class'     => 'nav-bottom-header'
	            ) ); ?>
            </div>
		<?php endif; ?>
		<?php if ( ! is_front_page() && ! is_page(282) && ! is_page(338) ) : ?>
            <style>

                @media screen and (min-width: 768px) {
                    .header-bottom {
                        min-height: 46px;
                    }
                    .header-main {
                        height: 30px;
                    }
                }
                #header .container{
                padding-left: 0;padding-right: 0;
                }


            </style>
		<?php endif; ?>
	    <?php if (  is_page(282) ) : ?>
            <style>

                @media screen and (min-width: 768px) {
                    #header .container {
                        padding-left: 0;
                        padding-right: 0;
                    }
                }


            </style>
	    <?php endif; ?>
        <!-- Mobile Right Elements -->
        <div class="flex-col show-for-medium flex-right">
            <ul class="mobile-nav nav nav-right <?php flatsome_nav_classes( 'main-mobile' ); ?>">
				<?php flatsome_header_elements( 'header_mobile_elements_right', 'mobile' ); ?>
            </ul>

        </div>

        <!--custom menu header bottom -->
<!--		--><?php //if (  is_front_page() ) : ?>
<!--            <div class="container header-bottom-menu">-->
<!--				--><?php //wp_nav_menu( array(
//					'theme_location' => 'bottom-header',
//					'menu_class'     => 'nav-bottom-header'
//				) ); ?>
<!--            </div>-->
<!--		--><?php //endif; ?>
    </div><!-- .header-inner -->

	<?php if ( get_theme_mod( 'header_divider', 1 ) ) { ?>
        <!-- Header divider -->
        <div class="container">
            <div class="top-divider full-width"></div>
        </div>
	<?php } ?>
</div><!-- .header-main -->