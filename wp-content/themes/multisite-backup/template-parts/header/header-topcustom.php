<?php if(!  wp_is_mobile() ): ?>
<div id="top-bar-custom" class="header-top hide-for-sticky nav-dark">
   <div class="flex-row container-topbar">
      <div class="flex-col hide-for-medium flex-left">
         <?php wp_nav_menu( array(
            'theme_location' => 'topbar-left',
            'menu_class' => 'nav nav-left medium-nav-center nav-small  nav-divided'
            ) ); ?>
      </div>
      <!-- flex-col left -->
      <div class="flex-col hide-for-medium flex-center">
         <ul class="nav nav-center nav-small  nav-divided">
         </ul>
      </div>
      <!-- center -->
      <div class="flex-col hide-for-medium flex-right right-custom-topbar">
          <?php if ( is_user_logged_in() ): ?>
          <div class="notifi">
	          <?php echo do_shortcode('[user_name]');?>
          </div>
          <?php endif; ?>
          <?php wp_nav_menu( array(
            'theme_location' => 'topbar-right',
            'menu_class' => 'nav top-bar-nav nav-right nav-small  nav-divided'
            ) ); ?>
      </div>
      <!-- .flex-col right -->
      <div class="flex-col show-for-medium flex-grow">
         <ul class="nav nav-center nav-small mobile-nav  nav-divided">
         </ul>
      </div>
   </div>

   <!-- .flex-row -->
</div>
    <?php if(is_page(282) || is_page(338)):  ?>
    <div class="flex-row container" id="quangcao-header">
        <div class="flex-col hide-for-medium flex-center">
	        <?php if ( is_active_sidebar( 'quangcao-header' ) ) :  dynamic_sidebar( 'quangcao-header' );  endif; ?>
        </div>
    </div>
    <?php endif; ?> 
<?php endif; ?> 