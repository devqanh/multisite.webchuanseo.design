<?php
/**
 * The template for displaying the footer.
 *
 * @package flatsome
 */

 global $jQueryflatsome_opt;
?>

</main><!-- #main -->

<footer id="footer" class="footer-wrapper">

	<?php do_action('flatsome_footer'); ?>

</footer><!-- .footer-wrapper -->

</div><!-- #wrapper -->

<?php  wp_footer(); ?>

<!--End of Tawk.to Script-->
</body>
</html>
