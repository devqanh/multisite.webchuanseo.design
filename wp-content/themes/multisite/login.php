<?php
/**
 * Template Name: Login
 */
if(get_current_user_id() > 0 ){
    wp_redirect(get_site_url().'/dashboard');
    die;
}


get_header();

do_action( 'flatsome_before_page' );
?>
<?php do_action( 'flatsome_after_page' ); ?>
    <div class="row">
        <div class="col large-6">
            <img src="http://yesone.com.tw/join/images/login.gif"/>
        </div>
        <div class="col large-6 form-login">
            <div class="form">
                <div class="row title-login">
                    <div class="col large-3">
                        Đăng nhập
                    </div>

                </div>
                
                <?php echo do_shortcode('[user_verification_check]'); ?> 
                <div class=" content-login">
                    <?php wp_login_form(array(
                        'redirect' => get_site_url() . '/dashboard',
                        'label_username' => 'Tên đăng nhập',
                        'label_password' => 'Mật khẩu',
                        'label_log_in' => 'Đăng nhập',
                        'label_remember' => 'Nhớ tôi'
                        
                    ));?>
 
            
                    <div class="link-dang-ky">
                        <a href="/dang-ky">Đăng ký tài khoản tại đây</a>
                    </div>
                </div>
        </div>
        <div class="footer-login">

        </div>
    </div><!-- .form -->
    </div>
    </div>
    <style>
        #main {
            margin-top: 30px;
        }

        .form-login {
            margin-top: 3%;
        }

        .form-login .form{
            border: 1px solid #b1abab;
            border-top-left-radius: 7px;
            border-top-right-radius: 7px;
        }

        .form-login .form .title-login, .form-login .content-login button , .form-login #wp-submit{
            color:gray;
            background: rgb(254, 254, 254); /* Old browsers */
            background: -moz-linear-gradient(top, rgba(254, 254, 254, 1) 25%, rgba(226, 226, 226, 1) 88%, rgba(226, 226, 226, 1) 88%); /* FF3.6-15 */
            background: -webkit-linear-gradient(top, rgba(254, 254, 254, 1) 25%, rgba(226, 226, 226, 1) 88%, rgba(226, 226, 226, 1) 88%); /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, rgba(254, 254, 254, 1) 25%, rgba(226, 226, 226, 1) 88%, rgba(226, 226, 226, 1) 88%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fefefe', endColorstr='#e2e2e2', GradientType=0); /* IE6-9 */
        }

        .form-login .form{
            overflow: hidden;
        }

        .form-login .title-login .col {
            padding: 5px 32px;
            background: #4190b7;
            color: #fff;
            margin: 2px 0 0px 16px;
            border-top-left-radius: 9px;
        }

        .form-login .content-login {
            padding: 30px 0px 10px 22px;
        }

        .form-login .link-dang-ky {
            border-top: 1px solid #a59e9e;
            width: 94%;
            text-align: center;
            padding-top: 11px;
            /* color: #4190b7; */
        }

        .form-login .link-dang-ky a {
            color: #4190b7;
        }

        .form-login .content-login button {

            border: 1px solid #bdacac;
        }
        .form-login .row.content-submit .col {
            padding: 0;
        }

        .form-login .row.content-submit {

            padding: 0 150px !important;
        }
        .form-login .check-ghi-nho {
            line-height: 38px;
        }

        .form-login form {
            overflow: hidden;
            border: none !important;
            padding-right: 16px;
            
        }

    </style>
<?php get_footer();
