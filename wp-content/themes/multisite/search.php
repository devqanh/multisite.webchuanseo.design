<?php
get_header(); 


$vendor_name = cuongdc_trim_strip($_GET['s']);

$args = array( 
  'meta_query' => array(
    array(
        'key' => 'dokan_store_name',
        'compare' => 'LIKE',
        'value' => $vendor_name
    ),
  ) 
);

$users = get_users( $args );



?>

<?php //do_action( 'flatsome_before_page' ); ?>

<div id="content-list-child" role="main" class="content-area">
	<div class="container">
		<div class="row brearcrumb">
			<?php get_flatsome_breadcrumbs(); ?>
		</div>
        <div class="gap-element" style="display:block; height:auto; padding-top:10px"></div>


        <div class="gap-element" style="display:block; height:auto; padding-top:30px"></div>



        <div class="gap-element" style="display:block; height:auto; padding-top:30px"></div>

        <div class="row section-title-custom">
            <h3 class="section-title section-title-normal">
                <b></b>
                <span class="section-title-main">Hiển thị kết quả tìm kiếm cho: <?php echo get_search_query();  ?></span>
                <b></b>
                <a href="#" target=""></a></a>
            </h3>
        </div>

        <div class="gap-element" style="display:block; height:auto; padding-top:30px"></div>

        <!-- START LIST THUONG HIEU  -->

<div id="main" class="list-thuong-hieu-wrapper" style="width:100%">
       


        <div id="main-left">
          <div class="main-left-content">

<?php 
           
            
            if( !empty( $users ) && is_array( $users ) ):

            foreach( $users as $u ):
            
            $cuaHang = cuongdc_get_user_thuonghieu_by_id( $u->ID );

?>
            <div class="main-content">
              <div class="main-content-new">
                <div class="main-content-left">
                    <a href="/b/<?php echo $cuaHang['store_slug'] ?>" title="<?php echo $cuaHang['store_settings']['store_name'] ?>">
                    <?php echo wp_get_attachment_image(  $cuaHang['store_settings']['gravatar'] , 'thumbnail', array(
                            'class' => 'thuonghieu-image'
                        ) ); ?>
                    </a>
                </div>
                <!--main-content-left-->
                <div class="main-content-center">
                  <a href="/b/<?php echo $cuaHang['store_slug'] ?>"><h2> <?php echo $cuaHang['store_settings']['store_name'] ?>  </h2></a>

                  <p><?php echo wp_trim_words( trim( strip_tags( $cuaHang['store_settings']['shop_short_intro'] ) ) , 120 , ' ...' )?></p>
                </div>
                <!---mian-content-center-->
                <div class="main-content-right">
                  <div class="main-content-right-info">
                    <h2>Yêu cầu</h2>
                  </div>
                  <!--main-content-right-info-->
                  <div class="main-content-right-btn">
                    <a>
                        <span style="display:none" class="uid"><?php echo $u->ID ?></span>
                        <button class="thuonghieu-btn-request" type="submit">Yêu cầu</button>
                            
                    </a>
                  </div>
                  <!--main-content-right-btn-->
                </div>
                <!--main-content-right-->
              </div>
              <!--main-content-new-->
            </div>
            <!--main-content-->
        <?php endforeach; 
              endif;
        ?>

            
          </div> <!--main-left-content-->


          <div class="main-left-list">
            <div class="row section-title-custom-sidebar">
              <h3 class="section-title section-title-normal">
                        <b></b>
                        <span class="section-title-main">Yêu cầu mới nhất</span>
                        <b></b>
                        <a href="/hoi-dap" target="_blank" style="font-size: 15px;margin-right: 20px;text-align:right;">Xem tất cả</a>
                    </h3>
            </div>

	          <?php
	          $the_query = new WP_Query(array(
		          'post_type' => 'wpcf7s' , //cotact form 7 submission post type
		          'posts_per_page' => 8,
		          'paged' =>  get_query_var('paged'),
	          ));
	          while( $the_query->have_posts() ):
		          $the_query->the_post();

		          $postId = get_the_Id();

		          if( empty( get_post_meta( $postId , 'wpcf7s_posted-ho-va-ten' , true  )  )
		              || empty( get_post_meta( $postId , 'wpcf7s_posted-loai-hinh-thuong-hieu' , true ) )  ) continue;

		          $nguoiHoi =         cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-ho-va-ten' , true  ) );
		          $sdt =              cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-tel-137' , true ) ) ;
		          $loaiThuongHieu =   cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-loai-hinh-thuong-hieu' , true )[0] );
		          $email  =           cuongdc_trim_strip( get_post_meta($postId, 'wpcf7s_posted-email-215' , true) );
		          $diaChiMoShop =      cuongdc_trim_strip( get_post_meta($postId, 'wpcf7s_posted-dia-chi-mo-shop' , true));
		          $matTien     =   cuongdc_trim_strip(get_post_meta($postId , 'wpcf7s_posted-mat-tien' , true )[0] ); // mật tiền
		          $dauTuFrom =      cuongdc_trim_strip(get_post_meta( $postId , 'wpcf7s_posted-du-kien-dau-tu-from' , true ) );
		          $dauTuTo   =    cuongdc_trim_strip( get_post_meta( $postId , 'wpcf7s_posted-du-kien-dau-tu-to'  , true) );
		          $thoiGianMo =     cuongdc_trim_strip(get_post_meta( $postId , 'wpcf7s_posted-du-kien-khai-truong' , true ) );
		          $thoiGianGap =   cuongdc_trim_strip(get_post_meta($postId , 'wpcf7s_posted-co-the-lien-he' , true ) );
		          $chuThich   =     cuongdc_trim_strip(get_post_meta( $postId , 'wpcf7s_posted-chu-thich'  , true ) );

		          ;

		          $hidden_email_arr = explode( "@" ,$email );
		          $hidden_email = substr_replace( $hidden_email_arr[0] , "******"  , 2) . "@".$hidden_email_arr[1];

		          $hidden_sdt = substr_replace( $sdt , "****" , 4 , (sizeof($sdt)-1) - 2 );


		          ?>
                  <div class="main-left-list-content modal-ask">
                      <span style="display:none" class="hidden-email"><?php echo $hidden_email ?></span>
                      <span style="display:none" class="hidden-sdt"><?php echo $hidden_sdt ?></span>
                      <a href="#">
                          <font color="#9A450E"><?php echo $nguoiHoi ?>：</font><?php echo $loaiThuongHieu ?>，Địa điểm：<?php echo $diaChiMoShop ?> ，Dự kiến khai trương：<?php echo $thoiGianMo ?> ，
                          <font color="red"><b>Kinh phí <?php echo $dauTuFrom ?>~<?php echo $dauTuTo ?> </b></font>
                      </a>
                  </div>
                  <!--main-left-list-content-->
	          <?php endwhile;
	          ?>


          </div>
          <!--main-left-list-->

          <div class="main-left-check" id="brand-hoi-dap" >
	          <?php echo do_shortcode('[contact-form-7 id="433" title="Hỏi đáp trực tuyến - Trang cá danh sách thương hiệu"]') ; ?>
          </div>
          <!--main-left-check-->

        </div>
        <!--main-left-->


        <!-- START MAIN RIGHT  -->
        <div id="main-right">
            <?php 
            
         if( !empty( $ads_right ) ):
           $ads_right = $loai_thuong_hieu['qcright_loop'];
                foreach( $ads_right as $ad_right_wrapper ):
            ?>
               <div class="ad-right-wrapper">
                  <div class="row section-title-custom-sidebar">
                    <h3 class="section-title section-title-normal">
                        <b></b>
                        <span class="section-title-main"><?php echo trim(strip_tags( $ad_right_wrapper['qcright_title'] ));?></span>
                        <b></b>
                    </h3>
                  </div>
                  <div class="main-right-bar">

                    <?php foreach( $ad_right_wrapper['qcright_ads'] as $ad ): 
                            if( empty( $ad['qcright_ads_image'] ) ){
                            ?>
                            <div class="main-left-list-content">
                              <a href="<?php echo cuongdc_trim_strip($ad['qcright_ads_link']);?>">
                                <font color="#9A450E"><?php echo cuongdc_trim_strip($ad['qcright_ads_title']);?></font>
                              </a>
                            </div>
                    <?php 
                            }else{
                    ?>
                        <div class="main-right-content">
                          <div class="main-right-content-img">
                            <a href="<?php echo cuongdc_trim_strip($ad['qcright_ads_link']) ?>" 
                                title="<?php echo cuongdc_trim_strip( $ad['qcright_ads_title'] ) ?>">
                                 <?php echo wp_get_attachment_image( $ad['qcright_ads_image'] , 'normal'  ) ?>
                             </a>
                          </div>
                          <!--mian-right-content-img-->
                          <div class="main-right-content-title">
                            <a href="<?php echo cuongdc_trim_strip($ad['qcright_ads_link']) ?>">
                                <h2><?php echo  cuongdc_trim_strip( $ad['qcright_ads_title']  );?></h2></a>
                          </div>
                          <!--main-right-content-title-->
                        </div>
                        <!--main-right-content-->
                    <?php };

                    endforeach?>

                  </div>
                  <!--main-right-bar-->
            </div><!-- .ad-right-wrapper -->
      <?php endforeach; endif; ?>
     

      <!--main-right-category-->

        </div><!-- main-right-wrapper -->
    </div>
        <!--main-right-->

      </div>
        <!-- END LIST THUONG HIEU  -->
	</div>
</div>




<div style="display:none"  class="modal" id="data-contact"><?php echo do_shortcode ('[contact-form-7 id="433" title="Hỏi đáp trực tuyến - Trang cá danh sách thương hiệu"]'); ?> </div>

<style type="text/css">

.main-left-list .main-left-list-content{
    height: auto !important;
}
.img-logo-thuong-hieu img{
    width: 220px;
height: 142px;
}
  .modal{
    max-width: 800px;
  }

  #form-qa .form-header input{
  margin-left: 0px;
}



  #form-qa .form-group{
  overflow: hidden;
  background: #FFFFE6;
}

#form-qa input{
  margin-left: 0px;
}

 #wide-nav .container {
     padding: 0;
 }


@media screen and ( min-width: 959px ){

#form-qa .form-group .form-label{
  width: 30%;
  float: left;
  display: block;

  
}

#form-qa .form-group .form-input{
  width: 70%;
  background: white;
  float: left;

}

#form-qa .form-header{
  color: #383835;
  text-align: center;
  width: 100%;
  background: #F3F3F3;
  padding: 6px;
}


#form-qa .du-kien-dau-tu-from, #form-qa .du-kien-dau-tu-to{
  float: left;

}

#form-qa .du-kien-dau-tu-to select, #form-qa .du-kien-dau-tu-from select{

  margin: 0px;
  padding-top : 5px;
  padding-bottom : 5px;
  font-size: 14px;
  padding-left: 10px;
  padding-right: 10px;
  
}


#form-qa .sep{

  width: 20px;
  float: left;
}

}/**959px **/

</style>
<script>
  (function($){
    $(document).ready(function(){
        console.log('brand ready! ');
        $(".thuonghieu-btn-request").on("click", function(){
            $("#uid").val( parseInt($(this).parent().find(".uid").text()) );
            $("#data-contact").modal();
        });
    });    
  })(jQuery);
</script> 


<?php while ( have_posts() ) : the_post(); ?>

	<?php the_content(); ?>

<?php endwhile; // end of the loop. ?>

<?php do_action( 'flatsome_after_page' ); ?>

<?php get_footer(); ?>

