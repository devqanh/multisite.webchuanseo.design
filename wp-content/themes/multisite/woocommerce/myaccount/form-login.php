<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php wc_print_notices(); ?>

<?php do_action( 'woocommerce_before_customer_login_form' ); ?>

<?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>
<div class="u-columns col2-set" id="customer_login">


<?php endif; ?>
<div class="row">
        <div class="col large-6">
            <img src="http://yesone.com.tw/join/images/login.gif"/>
        </div>
<?php if( empty( $_GET['action'] ) ): ?>
<div class="col large-6 form-login">
    <div class="form-login-boder">
                <div class="row title-login">
                    <div class="col large-3">
                        Đăng nhập
                    </div>

                </div>
        
		<form class="woocommerce-form woocommerce-form-login login content-login" method="post">

			<?php do_action( 'woocommerce_login_form_start' ); ?>

			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="username"><?php _e( 'Tên đăng nhập', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( $_POST['username'] ) : ''; ?>" />
			</p>
			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="password"><?php _e( 'Mật khẩu', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input class="woocommerce-Input woocommerce-Input--text input-text" type="password" name="password" id="password" />
			</p>

			<?php do_action( 'woocommerce_login_form' ); ?>

			<p class="form-row">
				<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>
				<input type="submit" class="woocommerce-Button button" name="login" value="<?php esc_attr_e( 'Login', 'woocommerce' ); ?>" />
				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox inline">
					<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php _e( 'Remember me', 'woocommerce' ); ?></span>
				</label>
			</p>
     <?php echo do_shortcode('[wordpress_social_login]') ?>

			<?php do_action( 'woocommerce_login_form_end' ); ?>
                        <div class="link-dang-ky">
                            <a href="?action=register">Đăng ký tài khoản tại đây</a>
                        </div>
		</form>
                          
    </div>
</div>
<?php else:?>
<div class="col large-6 form-login">
	<div class="u-column2 col-2">
		<form method="post" class="register form" >
                <div class="row title-login">
                    <div class="col large-3">
                        Đăng Ký
                    </div>

                </div>
                    <div class=" content-login">
			<?php do_action( 'woocommerce_register_form_start' ); ?>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="reg_username"><?php _e( 'Username', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( $_POST['username'] ) : ''; ?>" />
				</p>

			<?php endif; ?>

			<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
				<label for="reg_email"><?php _e( 'Địa chỉ Email', 'woocommerce' ); ?> <span class="required">*</span></label>
				<input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="reg_email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( $_POST['email'] ) : ''; ?>" />
			</p>

			<?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="reg_password"><?php _e( 'Mật khẩu', 'woocommerce' ); ?> <span class="required">*</span></label>
					<input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="reg_password" />
				</p>

			<?php endif; ?>

			<?php do_action( 'woocommerce_register_form' ); ?>

			<p class="woocommerce-FormRow form-row">
				<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
				<input type="submit" class="woocommerce-Button button" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>" />
        	</p>
        
          
			<?php do_action( 'woocommerce_register_form_end' ); ?>
			  <?php echo do_shortcode('[wordpress_social_login]') ?>
                    </div><!-- .content-login -->
		</form>
        
 
	</div>
</div>

<?php endif; ?>

<?php if( is_account_page()  ):?>
    <style>
        .page-wrapper{
            padding-top: 0;
        }
        .form-login {
            margin-top: 10%;
        }
        .form-login .form-login-boder , .register.form  {
            border: 1px solid #b1abab;
            border-top-left-radius: 7px;
            border-top-right-radius: 7px;
        }
        .form-login .content-login {
            border-top: 1px solid #b1abab;
        }
        .form-login form .title-login, .form-login .content-login button {
            background: rgb(254, 254, 254);
            /* Old browsers */
            background: -moz-linear-gradient(top, rgba(254, 254, 254, 1) 25%, rgba(226, 226, 226, 1) 88%, rgba(226, 226, 226, 1) 88%);
            /* FF3.6-15 */
            background: -webkit-linear-gradient(top, rgba(254, 254, 254, 1) 25%, rgba(226, 226, 226, 1) 88%, rgba(226, 226, 226, 1) 88%);
            /* Chrome10-25,Safari5.1-6 */
            background: linear-gradient(to bottom, rgba(254, 254, 254, 1) 25%, rgba(226, 226, 226, 1) 88%, rgba(226, 226, 226, 1) 88%);
            /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fefefe', endColorstr='#e2e2e2', GradientType=0);
            /* IE6-9 */
        }
        .form-login form {
            overflow: hidden;
        }
        .form-login .title-login .col {
            padding: 5px 26px;
            background: #4190b7;
            color: #fff;
            margin: 1px 0 0px 15px;
            border-top-left-radius: 9px;
        }
        .form-login .content-login {
            padding: 30px 0px 10px 22px;
        }
        .form-login .link-dang-ky {
            border-top: 1px solid #a59e9e;
            width: 94%;
            text-align: center;
            padding-top: 11px;
            /* color: #4190b7;
			 */
        }
        .form-login .link-dang-ky a {
            color: #4190b7;
        }
        .form-login .content-login button {
            border: 1px solid #bdacac;
        }
        .form-login .row.content-submit .col {
            padding: 0;
        }
        .form-login .row.content-submit {
            padding: 0 150px !important;
        }
        .form-login .check-ghi-nho {
            line-height: 38px;
        }
        .row{
            width:100% !important;
        }
    </style>
<?php endif;?>

</div><!-- .row -->
<?php do_action( 'woocommerce_after_customer_login_form' ); ?>
